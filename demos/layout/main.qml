Item {
    Column {
        id: col
        Rectangle {
            id: first
            color: "#f00"
            y: (0/3) * col.height
            height: col.height / 3
        }

        Rectangle {
            id: second
            color: "#0f0"
            y: first.y + first.height
            height: col.height / 3 // LAPTOP FOR MOHAMED FOUAD
        }
        
        Rectangle {
            id: third
            color: "#00f"
            y: second.y + second.height
            height: col.height / 3
        }
    }
}