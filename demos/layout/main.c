#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "../../lol/lol.h"

struct app_state_t
{
    lol_handle lol;
    bool mover_dragging;
    bool scaler_dragging;
};

static void
handle_mover(struct app_state_t* app_state, struct event_t e)
{
    size_t mover_mouse_area = engine_get_object_by_id(lol_engine(app_state->lol), "mover_mouse_area");
    size_t mover = engine_get_object_by_id(lol_engine(app_state->lol), "mover");

    if(!e.mouse_down)
    {
        app_state->mover_dragging = false;
    }

    if(app_state->mover_dragging)
    {
        int64_t mover_w = engine_object_width(lol_engine(app_state->lol), mover);
        int64_t mover_h = engine_object_height(lol_engine(app_state->lol), mover);

        engine_object_set_x(lol_engine(app_state->lol), mover, e.mouse_x - mover_w/2);
        engine_object_set_y(lol_engine(app_state->lol), mover, e.mouse_y - mover_h/2);
    }

    if(e.object_idx != mover_mouse_area)
    {
        return;
    }

    if(e.type == Event_Mouse_Down)
    {
        app_state->mover_dragging = true;
    }
}

static void
handle_scaler(struct app_state_t* app_state, struct event_t e)
{
    size_t scaler_mouse_area = engine_get_object_by_id(lol_engine(app_state->lol), "scaler_mouse_area");
    size_t scaler = engine_get_object_by_id(lol_engine(app_state->lol), "scaler");

    if(!e.mouse_down)
    {
        app_state->scaler_dragging = false;
    }

    if(app_state->scaler_dragging)
    {
        int64_t scaler_w = engine_object_width(lol_engine(app_state->lol), scaler);
        int64_t scaler_h = engine_object_height(lol_engine(app_state->lol), scaler);

        engine_object_set_x(lol_engine(app_state->lol), scaler, e.mouse_x - scaler_w/2);
        engine_object_set_y(lol_engine(app_state->lol), scaler, e.mouse_y - scaler_h/2);
    }

    if(e.object_idx != scaler_mouse_area)
    {
        return;
    }

    if(e.type == Event_Mouse_Down)
    {
        app_state->scaler_dragging = true;
    }
}

void
an_event_happened(void* user_data, struct event_t e)
{
    struct app_state_t* app_state = (struct app_state_t*)user_data;

    handle_mover(app_state, e);
    handle_scaler(app_state, e);
}


const char*
file_read(const char* const path)
{
    struct stat buf;
    int exists = (stat(path, &buf) == 0);

    if(!(S_ISREG(buf.st_mode) && exists))
    {
        return NULL;
    }

    if(buf.st_size < 0)
    {
        return NULL;
    }

    if(buf.st_size == 0)
    {
        return NULL;
    }

    int fd = open(path, O_RDONLY);

    if(fd < 0)
    {
        return NULL;
    }

    char* output = malloc(buf.st_size + 1);

    ssize_t bytes_read = read(fd, output, buf.st_size);

    output[buf.st_size] = 0;

    close(fd);

    if(bytes_read != buf.st_size)
    {
        free(output);
        return NULL;
    }

    return output;
}

int main()
{
    const char* const lol_content = file_read(src_dir "main.qml");

    if(lol_content == NULL)
    {
        return 1;
    }

    struct app_state_t app_state = {0};

    struct lol_config_t config = {
        .event_callback_function = an_event_happened,
        .user_data = &app_state
    };

    lol_handle lol = lol_init(config, src_dir, lol_content);

    if(lol == NULL)
    {
        return 1;
    }

    app_state.lol = lol;

    lol_run(lol);

    lol_release(lol);

    free((void*)lol_content);
}
