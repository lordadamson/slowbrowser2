Item {

    Rectangle {
        id: mover
        x: 100
        y: 100
        width: 30
        height: 30

        MouseArea {
            id: mover_mouse_area
            x: mover.x
            y: mover.y
            width: mover.width
            height: mover.height
        }
    }

    Image {
        id: img
        x: mover.x
        y: mover.y + mover.height
        width: scaler.x - x
        height: scaler.y - y
        src: "farmer_dev.png"
    }

    Rectangle {
        id: scaler
        x: img.x + img.implicit_width - width
        y: img.y + img.implicit_height
        width: 30
        height: 30

        MouseArea {
            id: scaler_mouse_area
            x: scaler.x
            y: scaler.y
            width: scaler.width
            height: scaler.height
        }
    }
}

