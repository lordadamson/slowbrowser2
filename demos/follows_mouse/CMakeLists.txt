cmake_minimum_required(VERSION 3.5)

project(demo LANGUAGES C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_STANDARD_REQUIRED TRUE)

set(exename follows_mouse)

add_executable(${exename}
    main.c
    main.qml
)

target_link_libraries(${exename}
    PRIVATE
    	lol
)

target_compile_definitions(${exename}
    PRIVATE
        "-Dsrc_dir=\"${CMAKE_CURRENT_SOURCE_DIR}/\""
)
