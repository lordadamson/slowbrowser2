Item {
    Rectangle {
        x: red.x - 5
        y: red.y - 5
        width: 150
        height: 100
        color: "#333"
    }

    Rectangle {
        id: red
        x: 55
        y: 55
        width: 150
        height: 100
        color: "#a22"

        MouseArea {
            id: red_mouse_area
            x: red.x
            y: red.y
            width: red.width
            height: red.height
        }
    }

    Text {
        text: "Hello World!"
        x: red.x
        y: red.y - height - 5
        height: 15
    }
}

