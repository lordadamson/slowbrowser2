#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "../../lol/lol.h"

struct app_state_t
{
    lol_handle lol;
    int current_color;
};

void
an_event_happened(void* user_data, struct event_t e)
{
    struct app_state_t* app_state = (struct app_state_t*)user_data;

    size_t red_mouse_area = engine_get_object_by_id(lol_engine(app_state->lol), "red_mouse_area");
    size_t red = engine_get_object_by_id(lol_engine(app_state->lol), "red");

    struct color_t colors[2] = {0};

    colors[0] = (struct color_t){
        .r = 255,
        .g = 0,
        .b = 0,
        .a = 255
    };

    colors[1] = (struct color_t){
        .r = 0,
        .g = 0,
        .b = 255,
        .a = 255
    };

    if(e.type == Event_Mouse_Pressed && e.object_idx == red_mouse_area)
    {
        struct color_t color = colors[app_state->current_color];
        app_state->current_color = !app_state->current_color;
        engine_object_set_color(lol_engine(app_state->lol), red, color);
    }
}


const char*
file_read(const char* const path)
{
    struct stat buf;
    int exists = (stat(path, &buf) == 0);

    if(!(S_ISREG(buf.st_mode) && exists))
    {
        return NULL;
    }

    if(buf.st_size < 0)
    {
        return NULL;
    }

    if(buf.st_size == 0)
    {
        return NULL;
    }

    int fd = open(path, O_RDONLY);

    if(fd < 0)
    {
        return NULL;
    }

    char* output = malloc(buf.st_size + 1);

    ssize_t bytes_read = read(fd, output, buf.st_size);

    output[buf.st_size] = 0;

    close(fd);

    if(bytes_read != buf.st_size)
    {
        free(output);
        return NULL;
    }

    return output;
}

int main()
{
    const char* const lol_content = file_read(src_dir "main.qml");

    if(lol_content == NULL)
    {
        return 1;
    }

    struct app_state_t app_state = {0};

    struct lol_config_t config = {
        .event_callback_function = an_event_happened,
        .user_data = &app_state
    };

    lol_handle lol = lol_init(config, src_dir, lol_content);

    app_state.lol = lol;

    lol_run(lol);

    lol_release(lol);

    free((void*)lol_content);
}
