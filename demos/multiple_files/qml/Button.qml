Rectangle {
    id: rect
    width: 30
    height: txt.height * 4

    Text {
        id: txt
        text: "Click me"
        x: rect.width / 2 - (height * 4)
        y: rect.height / 2 - height
        height: 12
    }

    MouseArea {
    }
}