#!/usr/bin/env python3

import math
from datetime import datetime
from datetime import timedelta

alpha1 = [
	("lexing", True),
	("parsing", True),
	("evaluation", True),
	("rendering rectangles", True),
	("evaluate math expressions", True),
	("do a symbol table as you evaluate", True),
	("vm to execute math expressions including references to attributes", True),
	("support property binding", True),
	("fix this readme", True),
	("C interface", True),
	("mouse events", True),
	("keyboard events", True),
	("proper directory structure", True),
	("support Text", True),
	("implement default values", True),
	("implement the engine functions", True),
	("support Mouse Area", True),
	("proper error handling", True),
	("support Images", True),
	("support multiple files", False),
	("Release alpha 1 for Linux", False),
	("Release alpha 1 for Windows", False),
	("Release alpha 1 for MacOS", False),
]

alpha2 = [
	("Figure out why resizing isn't smooth", False),
	("have a c function be called when a property changes", False),
	("Better error messages", False),
	("support text expressions", False),
	("serialize the engine", False),
	("make a build step for transpiling qml into c", False),
	("support RTL text", False),
	("TextInput", False),
	("support Rows and Columns", False),
	("impress ware: create a file splitter", False),
	("support anchors", False),
	("support margin", False),
	("support padding", False),
	("support visible bounding boxes", False),
	("support ternary operators", False),
	("support for boolean operations (and, or, xor, >, <, == ... etc)", False),
	("border property", False),
	("fill property", False),
	("support Ovals", False),
	("support import statements", False),
	("a naming convention that is followed all accross the codebase", False),
	("see what layouts are we going to support (flex, rows/colums, grid)", False),
	("drag events", False),
	("touch events", False),
	("run it on a mobile phone", False),
	("respect relationships (parents, children, siblings)", False),
	("impress ware: create a simple note taking app", False),
	("impress ware: create a toggle button that toggles light mode and dark mode but has animations and stuff", False),
	("Release alpha 2 for Linux", False),
	("Release alpha 2 for Windows", False),
	("Release alpha 2 for MacOS", False),
	("Release alpha 2 for Android", False),
	("Release alpha 2 for iOS", False),
]

beta1 = [
	("multi-touch events", False),
	("add transformation functions", False),
	("stress test it with logs of objects and optimize", False),
	("fuzzer for the lexer", False),
	("fuzzer for the parser", False),
	("fuzzer for the evaluater", False),
	("fuzzer for the engine", False),
	("C++ interface", False),
	("js interface", False),
	("python interface", False),
	("kotlin interface", False),
	("swift interface", False),
	("go interface", False),
	("Java interface", False),
	("rust interface", False),
	("C# interface", False),
	("lua interface", False),
	("make sure we are backward compatible", False),
	("make sure we are forward compatible", False),
	("add transformation functions", False),
	("impress ware: create a nodes editor (gollash, maybe?)", False),
	("Release beta 1 for Linux", False),
	("Release beta 1 for Windows", False),
	("Release beta 1 for MacOS", False),
	("Release beta 1 for Android", False),
	("Release beta 1 for iOS", False),
]

we_are_here = "   <---  **We are here**"

tasks_to_complete = 0

def gen_tasks(tasks, days_per_task):
	today = datetime.today()
	global tasks_to_complete
	output = ""
	finished_count = 0
	first_unfinished = True
	first_task = True

	for i in tasks:
		task, finished = i
		if finished:
			output += "- [x] "
			finished_count += 1
		else:
			output += "- [ ] "
			tasks_to_complete += 1

		output += task

		if not finished:
			days = days_per_task * tasks_to_complete
			eta = today + timedelta(days=days)
			output += " | ETA " + str(eta.strftime("%d/%m/%Y"))

		if finished_count > 0 and not finished and first_unfinished:
			output += we_are_here
			first_unfinished = False

		first_task = False
		
		output += "\n"

	output += "\n"
	return output, finished_count

def gen_section(tasks, section_name, days_per_task):
	output = "#### " + section_name + "\n\n"

	tasks_list, finished_count = gen_tasks(tasks, days_per_task)

	output += tasks_list

	progress = (float(finished_count) / float(len(tasks))) * 100

	output += "Progress: " + format(progress, '.2f') + "%\n"

	for i in range(finished_count):
		output += "✅"

	for i in range(len(tasks) - finished_count):
		output += "🔲"

	output += "\n\n"
	return output

f = open("README.md", 'r')

readme = f.read()

f.close()

to_find = '## Progress'

idx = readme.find(to_find)

readme = readme[:idx + len(to_find)]

days_per_task = 2

readme += """

This list is dynamic, and functions as a todo list for me,
so that if I remember something I wanna do, I wouldn't forget.
And the dates would give me motivation to achieve said things.
But it could will definitely be changing as time goes by.

"""

readme += gen_section(alpha1, "Alpha 1", days_per_task)
readme += gen_section(alpha2, "Alpha 2", days_per_task)
readme += gen_section(beta1, "Beta 1", days_per_task)

f = open("README.md", 'w')

print(readme, file=f)

f.close()