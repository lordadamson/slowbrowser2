# ugly_ui

It's a QML implementation, but without qt, aaand without js

![The best logo ever?](logo/naked_qml.jpg "The best logo ever?")

## Why?

- I think QML has done UI correctly
- I think it would be cool if you can use it without Qt

### Why naked_qml?

All the good qml features but without having to use Qt :D

- Hardware Accelerated
- Reusable modules
- dynamic property binding
- signals

## Progress

This list is dynamic, and functions as a todo list for me,
so that if I remember something I wanna do, I wouldn't forget.
And the dates would give me motivation to achieve said things.
But it could will definitely be changing as time goes by.

#### Alpha 1

- [x] lexing
- [x] parsing
- [x] evaluation
- [x] rendering rectangles
- [x] evaluate math expressions
- [x] do a symbol table as you evaluate
- [x] vm to execute math expressions including references to attributes
- [x] support property binding
- [x] fix this readme
- [x] C interface
- [x] mouse events
- [x] keyboard events
- [x] proper directory structure
- [x] support Text
- [x] implement default values
- [x] implement the engine functions
- [x] support Mouse Area
- [x] proper error handling
- [x] support Images
- [ ] support multiple files | ETA 10/06/2024   <---  **We are here**
- [ ] Release alpha 1 for Linux | ETA 12/06/2024
- [ ] Release alpha 1 for Windows | ETA 14/06/2024
- [ ] Release alpha 1 for MacOS | ETA 16/06/2024

Progress: 82.61%
✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅✅🔲🔲🔲🔲

#### Alpha 2

- [ ] Figure out why resizing isn't smooth | ETA 18/06/2024
- [ ] have a c function be called when a property changes | ETA 20/06/2024
- [ ] Better error messages | ETA 22/06/2024
- [ ] support text expressions | ETA 24/06/2024
- [ ] serialize the engine | ETA 26/06/2024
- [ ] make a build step for transpiling qml into c | ETA 28/06/2024
- [ ] support RTL text | ETA 30/06/2024
- [ ] TextInput | ETA 02/07/2024
- [ ] support Rows and Columns | ETA 04/07/2024
- [ ] impress ware: create a file splitter | ETA 06/07/2024
- [ ] support anchors | ETA 08/07/2024
- [ ] support margin | ETA 10/07/2024
- [ ] support padding | ETA 12/07/2024
- [ ] support visible bounding boxes | ETA 14/07/2024
- [ ] support ternary operators | ETA 16/07/2024
- [ ] support for boolean operations (and, or, xor, >, <, == ... etc) | ETA 18/07/2024
- [ ] border property | ETA 20/07/2024
- [ ] fill property | ETA 22/07/2024
- [ ] support Ovals | ETA 24/07/2024
- [ ] support import statements | ETA 26/07/2024
- [ ] a naming convention that is followed all accross the codebase | ETA 28/07/2024
- [ ] see what layouts are we going to support (flex, rows/colums, grid) | ETA 30/07/2024
- [ ] drag events | ETA 01/08/2024
- [ ] touch events | ETA 03/08/2024
- [ ] run it on a mobile phone | ETA 05/08/2024
- [ ] respect relationships (parents, children, siblings) | ETA 07/08/2024
- [ ] impress ware: create a simple note taking app | ETA 09/08/2024
- [ ] impress ware: create a toggle button that toggles light mode and dark mode but has animations and stuff | ETA 11/08/2024
- [ ] Release alpha 2 for Linux | ETA 13/08/2024
- [ ] Release alpha 2 for Windows | ETA 15/08/2024
- [ ] Release alpha 2 for MacOS | ETA 17/08/2024
- [ ] Release alpha 2 for Android | ETA 19/08/2024
- [ ] Release alpha 2 for iOS | ETA 21/08/2024

Progress: 0.00%
🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲

#### Beta 1

- [ ] multi-touch events | ETA 23/08/2024
- [ ] add transformation functions | ETA 25/08/2024
- [ ] stress test it with logs of objects and optimize | ETA 27/08/2024
- [ ] fuzzer for the lexer | ETA 29/08/2024
- [ ] fuzzer for the parser | ETA 31/08/2024
- [ ] fuzzer for the evaluater | ETA 02/09/2024
- [ ] fuzzer for the engine | ETA 04/09/2024
- [ ] C++ interface | ETA 06/09/2024
- [ ] js interface | ETA 08/09/2024
- [ ] python interface | ETA 10/09/2024
- [ ] kotlin interface | ETA 12/09/2024
- [ ] swift interface | ETA 14/09/2024
- [ ] go interface | ETA 16/09/2024
- [ ] Java interface | ETA 18/09/2024
- [ ] rust interface | ETA 20/09/2024
- [ ] C# interface | ETA 22/09/2024
- [ ] lua interface | ETA 24/09/2024
- [ ] make sure we are backward compatible | ETA 26/09/2024
- [ ] make sure we are forward compatible | ETA 28/09/2024
- [ ] add transformation functions | ETA 30/09/2024
- [ ] impress ware: create a nodes editor (gollash, maybe?) | ETA 02/10/2024
- [ ] Release beta 1 for Linux | ETA 04/10/2024
- [ ] Release beta 1 for Windows | ETA 06/10/2024
- [ ] Release beta 1 for MacOS | ETA 08/10/2024
- [ ] Release beta 1 for Android | ETA 10/10/2024
- [ ] Release beta 1 for iOS | ETA 12/10/2024

Progress: 0.00%
🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲🔲


