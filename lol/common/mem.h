#pragma once

#include <stddef.h>

#define MAX_ALLOCATORS 16

typedef int memory_handle;

struct memory_block_t
{
    void* mem;
    size_t size;
};

memory_handle
lol_memory_init(size_t size);

void
lol_memory_deinit(memory_handle);

struct memory_block_t
lol_alloc(memory_handle, size_t size);

void
lol_free(memory_handle, struct memory_block_t);
