#include "mem.h"

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

struct memory_context_t
{
    void* mem;
    size_t allocated;
    size_t size;
};

static struct memory_context_t allocators[MAX_ALLOCATORS];
static int full_allocators[MAX_ALLOCATORS];
static int allocators_count = 0;

#ifdef ctx
#undef ctx
#endif

#define ctx allocators[m]

static int
find_empty_allocator()
{
    for(size_t i = 0; i < MAX_ALLOCATORS; i++)
    {
        if(full_allocators[i] == 0)
        {
            return i;
        }
    }

    abort();
}

memory_handle
lol_memory_init(size_t size)
{
    struct memory_context_t allocator = {
        .mem = malloc(size),
        .size = size
    };

    memset(allocator.mem, 0, size);

    size_t allocator_index = allocators_count;

    if(allocators_count >= MAX_ALLOCATORS)
    {
        allocator_index = find_empty_allocator();
    }
    else
    {
        allocators_count++;
    }

    allocators[allocator_index] = allocator;

    return allocator_index;
}

struct memory_block_t
lol_alloc(memory_handle m, size_t size)
{
    assert(ctx.allocated + size <= ctx.size);

    struct memory_block_t block = {
        .mem = ctx.mem + ctx.allocated,
        .size  = size
    };

    ctx.allocated += size;

    return block;
}

void
lol_free(memory_handle m, struct memory_block_t block)
{
    ctx.allocated -= block.size;
}

void
lol_memory_deinit(memory_handle m)
{
    free(ctx.mem);
}
