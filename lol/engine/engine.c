#include "engine.h"

#include "../ui/lib/cat.h"

#include <string.h>
#include <stdlib.h>

#define STB_DS_IMPLEMENTATION
#include "../common/stb_ds.h"

size_t
engine_memory_needed()
{
    return sizeof(struct engine_t);
}

struct engine_t
engine_make()
{
    struct engine_t e = {0};
    return e;
}

void
engine_destroy(struct engine_t engine)
{
    arrfree(engine.objects);
}

size_t
engine_object_count(struct engine_t engine)
{
    return arrlenu(engine.objects);
}

size_t
engine_object_insert(struct engine_t* engine, struct object_t object)
{
    arrpush(engine->objects, object);
    return arrlenu(engine->objects) - 1;
}

void
engine_object_delete(struct engine_t engine, size_t me)
{
    // break the bindings
    for(size_t i = 0; i < engine_object_count(engine); i++)
    {
        struct object_t obj = engine.objects[i];

        if(obj.bindings == NULL)
        {
            continue;
        }

        for(size_t j = 0; j < arrlenu(obj.bindings); j++)
        {
            if(engine.objects[i].bindings[j].bound_to != me)
            {
                continue;
            }

            switch(engine.objects[i].bindings[j].field)
            {
                case Field_Width:
                {
                    int64_t w = engine_object_width(engine, j);
                    engine_object_set_width(engine, j, w);
                    break;
                }
                case Field_Height:
                {
                    int64_t h = engine_object_height(engine, j);
                    engine_object_set_height(engine, j, h);
                    break;
                }
                case Field_X:
                {
                    int64_t x = engine_object_x(engine, j);
                    engine_object_set_x(engine, j, x);
                    break;
                }
                case Field_Y:
                {
                    int64_t y = engine_object_y(engine, j);
                    engine_object_set_y(engine, j, y);
                    break;
                }
                case Field_Color:
                {
                    struct color_t c = engine_object_color(engine, j);
                    engine_object_set_color(engine, j, c);
                    break;
                }
                case Field_Text:
                {
                    struct string_t t = engine_object_text(engine, j);
                    engine_object_set_text(engine, j, t);
                    break;
                }
                case Field_Attribute:
                {
                    struct string_t t = engine_object_text(engine, j);
                    engine_object_set_text(engine, j, t);
                    break;
                }
                default: abort();
            }
        }
    }

    // delete children
    if(engine.objects[me].children != NULL)
    {
        for(size_t i = 0; i < arrlenu(engine.objects[me].children); i++)
        {
            engine_object_delete(engine, engine.objects[me].children[i]);
        }
    }

    arrdel(engine.objects, me);
}

void engine_object_set_child(struct engine_t engine, size_t parent_idx, size_t child_idx)
{
    arrpush(engine.objects[parent_idx].children, child_idx);
}

void engine_object_set_width(struct engine_t engine, size_t object_idx, uint32_t value)
{
    struct object_t* object = &engine.objects[object_idx];

    object->width.value.integer = value;
    object->width.type = Attribute_Integer;
}

void engine_object_set_height(struct engine_t engine, size_t object_idx, uint32_t value)
{
    struct object_t* object = &engine.objects[object_idx];

    object->height.value.integer = value;
    object->height.type = Attribute_Integer;
}

void engine_object_set_x(struct engine_t engine, size_t object_idx, uint32_t value)
{
    struct object_t* object = &engine.objects[object_idx];

    object->x.value.integer = value;
    object->x.type = Attribute_Integer;
}

void engine_object_set_y(struct engine_t engine, size_t object_idx, uint32_t value)
{
    struct object_t* object = &engine.objects[object_idx];

    object->y.value.integer = value;
    object->y.type = Attribute_Integer;
}

void engine_object_set_color(struct engine_t engine, size_t object_idx, struct color_t value)
{
    struct object_t* object = &engine.objects[object_idx];

    object->color = value;
}

void engine_object_set_text(struct engine_t engine, size_t object_idx, struct string_t value)
{
    struct object_t* object = &engine.objects[object_idx];

    object->text.type = Attribute_String;
    object->text.value.string = value;
}

void engine_object_set_attribute(struct engine_t engine, size_t object_idx, struct string_t attribute, struct attribute_value_t value)
{
    struct object_t* object = &engine.objects[object_idx];

    struct object_attribute_t a = {
        .key = attribute,
        .value = value
    };

    arrpush(object->attributes, a);
}

void engine_object_bind_attributes(struct engine_t engine, size_t object_idx, struct binding_t binding)
{
    struct object_t* object = &engine.objects[object_idx];

    arrpush(object->bindings, binding);
}

int64_t
engine_get_object_by_id(struct engine_t engine, const char* id)
{
    for(size_t i = 0; i < engine_object_count(engine); i++)
    {
        if(strcmp(engine.objects[i].id.string, id) == 0)
        {
            return i;
        }
    }

    return -1;
}

struct attribute_value_t
attribute_make_i(enum attribute_type type, int64_t value)
{
    return (struct attribute_value_t){
        .type = type,
        .value.integer = value
    };
}

struct attribute_value_t
attribute_make_r(enum attribute_type type, double value)
{
    return (struct attribute_value_t){
        .type = type,
        .value.real = value
    };
}

struct attribute_value_t
attribute_make_b(enum attribute_type type, bool value)
{
    return (struct attribute_value_t){
        .type = type,
        .value.boolean = value
    };
}

struct vm_t
{
    struct engine_t const engine;
    size_t obj_idx;
    struct bytecode_unit_t stack[16];
    size_t stack_size;
};

static struct bytecode_unit_t
add(struct bytecode_unit_t arg1, struct bytecode_unit_t arg2)
{
    struct bytecode_unit_t result = {0};

    if(arg1.type == BYTECODE_UNIT_STRING && arg2.type != BYTECODE_UNIT_STRING)
    {
        abort();
    }

    if(arg2.type == BYTECODE_UNIT_STRING && arg1.type != BYTECODE_UNIT_STRING)
    {
        abort();
    }

    if(arg1.type == BYTECODE_UNIT_INT && arg2.type == BYTECODE_UNIT_INT)
    {
        result.type = BYTECODE_UNIT_INT;
        result.value.integer = arg1.value.integer + arg2.value.integer;
    }
    else if(arg1.type == BYTECODE_UNIT_REAL || arg2.type == BYTECODE_UNIT_REAL)
    {
        result.type = BYTECODE_UNIT_REAL;

        if(arg1.type == BYTECODE_UNIT_REAL && arg2.type == BYTECODE_UNIT_REAL)
        {
            result.value.real = arg1.value.real + arg2.value.real;
        }
        else if(arg1.type == BYTECODE_UNIT_REAL && arg2.type == BYTECODE_UNIT_INT)
        {
            result.value.real = arg1.value.real + arg2.value.integer;
        }
        else if(arg1.type == BYTECODE_UNIT_INT && arg2.type == BYTECODE_UNIT_REAL)
        {
            result.value.real = arg1.value.integer + arg2.value.real;
        }
        else
        {
            abort();
        }

        return result;
    }
    else if(arg1.type == BYTECODE_UNIT_STRING || arg2.type == BYTECODE_UNIT_STRING)
    {
        result.type = BYTECODE_UNIT_STRING;
        strncpy(result.value.string.string, arg1.value.string.string, arg1.value.string.length);
        strncpy(result.value.string.string + arg1.value.string.length, arg2.value.string.string, arg2.value.string.length);
        result.value.string.length = arg1.value.string.length + arg2.value.string.length;
        return result;
    }

    return result;
}

static struct bytecode_unit_t
sub(struct bytecode_unit_t arg1, struct bytecode_unit_t arg2)
{
    struct bytecode_unit_t result = {0};

    if(arg1.type == BYTECODE_UNIT_STRING || arg2.type == BYTECODE_UNIT_STRING)
    {
        abort();
    }

    if(arg1.type == BYTECODE_UNIT_REAL || arg2.type == BYTECODE_UNIT_REAL)
    {
        result.type = BYTECODE_UNIT_REAL;
    }
    else
    {
        result.type = BYTECODE_UNIT_INT;
    }

    if(arg1.type == BYTECODE_UNIT_REAL && arg2.type == BYTECODE_UNIT_REAL)
    {
        result.value.real = arg1.value.real - arg2.value.real;
    }
    else if(arg1.type == BYTECODE_UNIT_INT && arg2.type == BYTECODE_UNIT_INT)
    {
        result.value.integer = arg1.value.integer - arg2.value.integer;
    }
    else if(arg1.type == BYTECODE_UNIT_REAL && arg2.type == BYTECODE_UNIT_INT)
    {
        result.value.real = arg1.value.real - arg2.value.integer;
    }
    else if(arg1.type == BYTECODE_UNIT_INT && arg2.type == BYTECODE_UNIT_REAL)
    {
        result.value.real = arg1.value.integer - arg2.value.real;
    }
    else
    {
        abort();
    }

    return result;
}

static struct bytecode_unit_t
mul(struct bytecode_unit_t arg1, struct bytecode_unit_t arg2)
{
    struct bytecode_unit_t result = {0};

    if(arg1.type == BYTECODE_UNIT_STRING || arg2.type == BYTECODE_UNIT_STRING)
    {
        abort();
    }

    if(arg1.type == BYTECODE_UNIT_REAL || arg2.type == BYTECODE_UNIT_REAL)
    {
        result.type = BYTECODE_UNIT_REAL;
    }
    else
    {
        result.type = BYTECODE_UNIT_INT;
    }

    if(arg1.type == BYTECODE_UNIT_REAL && arg2.type == BYTECODE_UNIT_REAL)
    {
        result.value.real = arg1.value.real * arg2.value.real;
    }
    else if(arg1.type == BYTECODE_UNIT_INT && arg2.type == BYTECODE_UNIT_INT)
    {
        result.value.integer = arg1.value.integer * arg2.value.integer;
    }
    else if(arg1.type == BYTECODE_UNIT_REAL && arg2.type == BYTECODE_UNIT_INT)
    {
        result.value.real = arg1.value.real * arg2.value.integer;
    }
    else if(arg1.type == BYTECODE_UNIT_INT && arg2.type == BYTECODE_UNIT_REAL)
    {
        result.value.real = arg1.value.integer * arg2.value.real;
    }
    else
    {
        abort();
    }

    return result;
}

static struct bytecode_unit_t
divv(struct bytecode_unit_t arg1, struct bytecode_unit_t arg2)
{
    struct bytecode_unit_t result = {0};

    if(arg1.type == BYTECODE_UNIT_STRING || arg2.type == BYTECODE_UNIT_STRING)
    {
        abort();
    }

    result.type = BYTECODE_UNIT_REAL;

    double epsilon = 0.000000001;

    if(arg1.type == BYTECODE_UNIT_REAL && arg2.type == BYTECODE_UNIT_REAL)
    {
        result.value.real = arg1.value.real / (arg2.value.real + epsilon);
    }
    else if(arg1.type == BYTECODE_UNIT_INT && arg2.type == BYTECODE_UNIT_INT)
    {
        result.value.real = (double)arg1.value.integer / ((double)arg2.value.integer + epsilon);
    }
    else if(arg1.type == BYTECODE_UNIT_REAL && arg2.type == BYTECODE_UNIT_INT)
    {
        result.value.real = arg1.value.real / ((double)arg2.value.integer + epsilon);
    }
    else if(arg1.type == BYTECODE_UNIT_INT && arg2.type == BYTECODE_UNIT_REAL)
    {
        result.value.real = (double)arg1.value.integer / (arg2.value.real + epsilon);
    }
    else
    {
        abort();
    }

    return result;
}

static struct bytecode_unit_t
pop(struct vm_t* vm, struct bytecode_unit_t arg)
{
    if(arg.type == BYTECODE_UNIT_POP)
    {
        arg = vm->stack[vm->stack_size-1];
        vm->stack_size--;
        return arg;
    }

    return arg;
}

static struct bytecode_unit_t
make_bytecode_unit_i(int64_t value)
{
    struct bytecode_unit_t output = {
        .type = BYTECODE_UNIT_INT,
        .value.integer = value
    };

    return output;
}

static struct bytecode_unit_t
make_bytecode_unit_d(double value)
{
    struct bytecode_unit_t output = {
        .type = BYTECODE_UNIT_REAL,
        .value.real = value
    };

    return output;
}

static struct bytecode_unit_t
make_bytecode_unit_s(struct string_t value)
{
    struct bytecode_unit_t output = {
        .type = BYTECODE_UNIT_STRING,
        .value.string = value
    };

    return output;
}

static int
find(char* string, char c)
{
    size_t length = strlen(string);

    for(size_t i = 0; i < length; i++)
    {
        if(string[i] == c)
        {
            return i;
        }
    }

    return -1;
}

static struct bytecode_unit_t
evaluate_attribute(struct engine_t const engine, size_t object_idx, struct attribute_value_t a)
{
    struct bytecode_unit_t result = {0};

    switch(a.type)
    {
    case Attribute_Integer:
        result.type = BYTECODE_UNIT_INT;
        result.value.integer = a.value.integer;
        return result;
    case Attribute_Real:
        result.type = BYTECODE_UNIT_REAL;
        result.value.real = a.value.real;
        return result;
    case Attribute_Boolean:
        result.type = BYTECODE_UNIT_BOOL;
        result.value.boolean = a.value.boolean;
        return result;
    case Attribute_String:
        result.type = BYTECODE_UNIT_STRING;
        result.value.string = a.value.string;
        return result;
    case Attribute_Bytecode:
        result = bytecode_execute(engine, object_idx, a.value.instructions);
        return result;
    default: abort();
    }
}

static struct bytecode_unit_t
deref(struct vm_t* vm, struct bytecode_unit_t arg)
{
    struct bytecode_unit_t output = {0};

    switch(arg.type)
    {
    case BYTECODE_UNIT_INT:
    case BYTECODE_UNIT_REAL:
    case BYTECODE_UNIT_STRING:
        return arg;
    case BYTECODE_UNIT_ATTRIBUTE:
        switch(arg.value.attribute.type)
        {
        case Attribute_Reference_Window_Width:
            output.type = BYTECODE_UNIT_INT;
            output.value.integer = cat_window_width(vm->engine.cat);
            return output;
        case Attribute_Reference_Window_Height:
            output.type = BYTECODE_UNIT_INT;
            output.value.integer = cat_window_height(vm->engine.cat);
            return output;
        case Attribute_Reference_Other:
            output = evaluate_attribute(vm->engine, vm->obj_idx, *arg.value.attribute.ref);
            return output;
        default: abort();
        }
    default: abort();
    }
}

static struct bytecode_unit_t
pop_and_deref(struct vm_t* vm, struct bytecode_unit_t arg)
{
    arg = pop(vm, arg);

    if(arg.type == BYTECODE_UNIT_ATTRIBUTE)
    {
        return deref(vm, arg);
    }

    return arg;
}

struct bytecode_unit_t
bytecode_execute_add(struct vm_t* vm, struct bytecode_unit_t arg1, struct bytecode_unit_t arg2)
{
    struct bytecode_unit_t result = {0};

    arg1 = pop_and_deref(vm, arg1);
    arg2 = pop_and_deref(vm, arg2);

    return add(arg1, arg2);
}

struct bytecode_unit_t
bytecode_execute_sub(struct vm_t* vm, struct bytecode_unit_t arg1, struct bytecode_unit_t arg2)
{
    struct bytecode_unit_t result = {0};

    arg1 = pop_and_deref(vm, arg1);
    arg2 = pop_and_deref(vm, arg2);

    return sub(arg1, arg2);
}

struct bytecode_unit_t
bytecode_execute_mul(struct vm_t* vm, struct bytecode_unit_t arg1, struct bytecode_unit_t arg2)
{
    struct bytecode_unit_t result = {0};

    arg1 = pop_and_deref(vm, arg1);
    arg2 = pop_and_deref(vm, arg2);

    return mul(arg1, arg2);
}

struct bytecode_unit_t
bytecode_execute_div(struct vm_t* vm, struct bytecode_unit_t arg1, struct bytecode_unit_t arg2)
{
    struct bytecode_unit_t result = {0};

    arg1 = pop_and_deref(vm, arg1);
    arg2 = pop_and_deref(vm, arg2);

    return divv(arg1, arg2);
}

struct bytecode_unit_t
bytecode_execute(struct engine_t const engine, size_t obj_idx, struct bytecode_t bc)
{
    struct bytecode_unit_t result;
    struct bytecode_unit_t tmp;

    struct vm_t vm = {
        .engine = engine,
        .obj_idx = obj_idx,
        .stack_size = 0,
        .stack = {0}
    };

    for(size_t i = 0; i < bc.count; i++)
    {
        switch(bc.instructions[i].type)
        {
            case BYTECODE_ADD:
                tmp = bytecode_execute_add(&vm, bc.instructions[i].args[0], bc.instructions[i].args[1]);
                break;
            case BYTECODE_SUB:
                tmp = bytecode_execute_sub(&vm, bc.instructions[i].args[0], bc.instructions[i].args[1]);
                break;
            case BYTECODE_MUL:
                tmp = bytecode_execute_mul(&vm, bc.instructions[i].args[0], bc.instructions[i].args[1]);
                break;
            case BYTECODE_DIV:
                tmp = bytecode_execute_div(&vm, bc.instructions[i].args[0], bc.instructions[i].args[1]);
                break;
            case BYTECODE_PUSH:
                tmp = bc.instructions[i].args[0];
                tmp = deref(&vm, tmp);
                break;
            default: abort();
        }

        vm.stack[vm.stack_size++] = tmp;
    }

    assert(vm.stack_size == 1);

    result = vm.stack[0];
    return result;
}

void
bytecode_instruction_push(struct bytecode_t* bytecode, struct bytecode_instruction_t instruction)
{
    bytecode->instructions[bytecode->count++] = instruction;
}

struct string_t
string_from_char_array(const char* input)
{
    struct string_t output = {0};
    strcpy(output.string, input);
    output.length = strlen(input);
    return output;
}

struct string_t
string_from_char_array_and_len(const char* input, size_t len)
{
    struct string_t output = {0};
    strncpy(output.string, input, len);
    output.length = len;
    return output;
}

void
string_append(struct string_t* s, const char* const to_append)
{
    size_t to_append_len = strlen(to_append);

    if(s->length + to_append_len >= MAX_STRING_LENGTH)
    {
        abort();
    }

    strcpy(s->string + s->length, to_append);
    s->length += to_append_len;
}

static int64_t
round(double d)
{
    d = d + 0.5 - (d<0);
    return (int64_t)d;
}

static int64_t
evaluate_attribute_as_int(struct engine_t const engine, size_t object_idx, struct attribute_value_t a)
{
    if(a.type == Attribute_Integer)
    {
        return a.value.integer;
    }
    else if(a.type == Attribute_Bytecode)
    {
        struct bytecode_unit_t result = bytecode_execute(engine, object_idx, a.value.instructions);

        switch (result.type)
        {
            case BYTECODE_UNIT_INT: return result.value.integer;
            case BYTECODE_UNIT_REAL: return round(result.value.real);
            default: abort();
        }
    }
    else
    {
        abort();
    }
}

static struct string_t
evaluate_attribute_as_string(struct engine_t const engine, size_t object_idx, struct attribute_value_t a)
{
    if(a.type == Attribute_String)
    {
        return a.value.string;
    }
    else if(a.type == Attribute_Bytecode)
    {
        struct bytecode_unit_t result = bytecode_execute(engine, object_idx, a.value.instructions);

        if(result.type != BYTECODE_UNIT_STRING)
        {
            abort();
        }

        return result.value.string;
    }
    else
    {
        abort();
    }
}

struct color_t
engine_object_color(const struct engine_t engine, size_t object_idx)
{
    struct object_t obj = engine.objects[object_idx];

    return obj.color;
}

int64_t
engine_object_x(struct engine_t const engine, size_t object_idx)
{
    struct object_t obj = engine.objects[object_idx];

    return evaluate_attribute_as_int(engine, object_idx, obj.x);
}

int64_t
engine_object_y(struct engine_t const engine, size_t object_idx)
{
    struct object_t obj = engine.objects[object_idx];

    return evaluate_attribute_as_int(engine, object_idx, obj.y);
}

struct Texture*
engine_object_texture(struct engine_t const engine, size_t object_idx)
{
    struct object_t obj = engine.objects[object_idx];

    return obj.texture;
}

int64_t
engine_object_width(struct engine_t const engine, size_t object_idx)
{
    struct object_t obj = engine.objects[object_idx];

    return evaluate_attribute_as_int(engine, object_idx, obj.width);
}

int64_t
engine_object_height(struct engine_t const engine, size_t object_idx)
{
    struct object_t obj = engine.objects[object_idx];

    return evaluate_attribute_as_int(engine, object_idx, obj.height);
}

int64_t
engine_object_implicit_width(struct engine_t const engine, size_t object_idx)
{
    struct object_t obj = engine.objects[object_idx];

    return evaluate_attribute_as_int(engine, object_idx, obj.implicit_width);
}

int64_t
engine_object_implicit_height(struct engine_t const engine, size_t object_idx)
{
    struct object_t obj = engine.objects[object_idx];

    return evaluate_attribute_as_int(engine, object_idx, obj.implicit_height);
}

struct string_t
engine_object_text(struct engine_t const engine, size_t object_idx)
{
    struct object_t obj = engine.objects[object_idx];

    return evaluate_attribute_as_string(engine, object_idx, obj.text);
}

struct attribute_value_t
engine_object_attribute(struct engine_t const engine, size_t object_idx, struct string_t key)
{
    struct object_t obj = engine.objects[object_idx];

    struct attribute_value_t output = {0};

    if(obj.attributes == NULL)
    {
        return output;
    }

    for(size_t i = 0; i < arrlenu(obj.attributes); i++)
    {
        if(strcmp(key.string, obj.attributes[i].key.string) == 0)
        {
            return obj.attributes[i].value;
        }
    }

    return output;
}
