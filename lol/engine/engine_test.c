#include "engine.h"

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#include <sys/time.h>

// TODO(adham): find a better way to measure, I really doubt that
// 				gettimeofday is the function I should be using
#define measure_time_start(name) \
        struct timeval name##begin = {0}; \
        gettimeofday(&name##begin, NULL)

#define measure_time_end(name) \
        struct timeval name##end = {0}; \
        gettimeofday(&name##end, NULL); \
        printf(#name " took %d microseconds to execute\n", name##end.tv_usec - name##begin.tv_usec)
int
random_between(int from, int to)
{
    //return arc4random_uniform(to+1) + from;
    return (from + rand()) % to;
}

void insert(struct engine_t engine)
{
	int i = 1000;

    measure_time_start(insert);
	while(i --> 0)
	{
		struct object_t object;
        engine_object_insert(&engine, object);
	}
    measure_time_end(insert);
}

void delete(struct engine_t engine)
{
    measure_time_start(delete);
    for(size_t i = 0; i < engine_object_count(engine); i++)
	{
        engine_object_delete(engine, i);
	}
    measure_time_end(delete);
}

void parents(struct engine_t engine)
{
    measure_time_start(parents);
    for(size_t i = 0; i < engine_object_count(engine); i++)
	{
		size_t children_count = random_between(0, 1000);

		for(size_t j = 0; j < children_count; j++)
		{
            size_t child = random_between(0, engine_object_count(engine) - 1);
            engine_object_set_child(engine, i, child);
		}
	}
    measure_time_end(parents);
}

void attributes(struct engine_t engine)
{
	// todo(adham): get the random generation functions outside
	// 				as to not affect the time measurement
    measure_time_start(attributes);
    for(size_t i = 0; i < engine_object_count(engine); i++)
	{
        struct attribute_value_t att = attribute_make_i(Attribute_Integer, 42);
        engine_object_set_attribute(engine, i, string_from_char_array("dummy_value"), att);
	}
    measure_time_end(attributes);
}

void change_attribute(struct engine_t engine)
{
	// todo(adham): get the random generation functions outside
	// 				as to not affect the time measurement
    measure_time_start(change_attribute);
    for(size_t i = 0; i < engine_object_count(engine); i++)
	{
        struct attribute_value_t att = attribute_make_i(Attribute_Integer, 69);
        engine_object_set_attribute(engine, i, string_from_char_array("dummy_value"), att);
	}
    measure_time_end(change_attribute);
}

void data_binding(struct engine_t engine)
{
    struct string_t dummy = string_from_char_array("dummy_value");

	// todo(adham): get the random generation functions outside
	// 				as to not affect the time measurement
    measure_time_start(data_binding);
    for(size_t i = 0; i < engine_object_count(engine); i++)
	{
		size_t binding_against_count = random_between(0, 100);

		while(binding_against_count--)
		{
            size_t binding_against = random_between(0, engine_object_count(engine) - 1);

            struct binding_t b = {
                .bound_to = binding_against,
                .attribute_key = dummy,
                .field = Field_Attribute
            };

            engine_object_bind_attributes(engine, i, b);
		}
	}
    measure_time_end(data_binding);
}

int main()
{
	struct engine_t engine = {0};

    insert(engine);
	
    delete(engine);

    memset(&engine, 0, sizeof(engine));

    insert(engine);

    parents(engine);

    delete(engine);

    memset(&engine, 0, sizeof(engine));

    insert(engine);

    data_binding(engine);

    delete(engine);

    memset(&engine, 0, sizeof(engine));

    insert(engine);

    parents(engine);

    attributes(engine);

    data_binding(engine);

    delete(engine);

    memset(&engine, 0, sizeof(engine));

    insert(engine);

    parents(engine);

    attributes(engine);

    change_attribute(engine);

    data_binding(engine);

    delete(engine);

    memset(&engine, 0, sizeof(engine));

    // TODO(adham): test performance of engine_get_object_by_id
}
