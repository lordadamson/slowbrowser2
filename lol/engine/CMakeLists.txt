cmake_minimum_required(VERSION 3.5)

project(engine LANGUAGES C)

add_library(engine
    engine.c
    engine.h
)

add_executable(engine_test
    engine_test.c
)

target_link_libraries(engine_test
    PRIVATE
        engine
        cat
)
