#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#define MAX_UI_OBJECTS 16384
#define MAX_STRING_LENGTH 256
#define MAX_ATTRIBUTES 4
#define MAX_INSTRUCTIONS 32

struct string_t
{
    char string[MAX_STRING_LENGTH];
    size_t length;
};

struct string_t
string_from_char_array(const char*);

struct string_t
string_from_char_array_and_len(const char*, size_t);

void
string_append(struct string_t* s, const char* const to_append);

enum field_e {
    Field_None,
    Field_Color,
    Field_Width,
    Field_Height,
    Field_X,
    Field_Y,
    Field_Text,
    Field_Attribute
};

struct attribute_value_t;

struct attribute_reference_t
{
    enum attribute_reference_type {
        Attribute_Reference_Window_None,
        Attribute_Reference_Window_Width,
        Attribute_Reference_Window_Height,
        Attribute_Reference_Other,
    } type;

    struct attribute_value_t* ref;
};

struct bytecode_unit_t
{
    enum bytecode_unit_type {
        BYTECODE_UNIT_NONE,
        BYTECODE_UNIT_INT,
        BYTECODE_UNIT_REAL,
        BYTECODE_UNIT_STRING,
        BYTECODE_UNIT_BOOL,
        BYTECODE_UNIT_ATTRIBUTE,
        BYTECODE_UNIT_POP
    } type;

    union bytecode_unit_value {
        bool boolean;
        int64_t integer;
        double real;
        struct string_t string;
        struct attribute_reference_t attribute;
    } value;
};

struct bytecode_instruction_t
{
    enum InstructionType {
        BYTECODE_NONE,
        BYTECODE_ADD,
        BYTECODE_SUB,
        BYTECODE_MUL,
        BYTECODE_DIV,
        BYTECODE_PUSH
    } type;

    struct bytecode_unit_t args[2];
};

struct bytecode_t
{
    struct bytecode_instruction_t instructions[MAX_INSTRUCTIONS];
    size_t count;
};

struct attribute_value_t
{
    enum attribute_type {
        Attribute_None,
        Attribute_Integer,
        Attribute_Real,
        Attribute_Boolean,
        Attribute_String,
        Attribute_Bytecode,
    } type;

    union value_t {
        int64_t integer;
        double real;
        bool boolean;
        struct string_t string;
        struct bytecode_t instructions;
    } value;
};

struct color_t
{
    uint8_t r, g, b, a;
};

struct object_attribute_t
{
    struct string_t key;
    struct attribute_value_t value;
};

struct binding_t
{
    size_t bound_to;

    enum field_e field;

    struct string_t attribute_key;
};

struct Texture;
struct cat_t;

struct object_t
{
    struct string_t id;

    enum object_type {
        Object_None,
        Object_Item,
        Object_Rectangle,
        Object_Oval,
        Object_Text,
        Object_Image,
        Object_MouseArea,
        Object_Row,
        Object_Column
    } type;

    struct color_t color;
    struct attribute_value_t width;
    struct attribute_value_t height;
    struct attribute_value_t implicit_width;
    struct attribute_value_t implicit_height;
    struct attribute_value_t x;
    struct attribute_value_t y;
    struct attribute_value_t text;
    bool visible;
    struct Texture* texture;

    struct cat_t* cat;

    size_t attributes_count;
    struct object_attribute_t* attributes;

    int32_t parent;
    size_t* children;
    struct binding_t* bindings;
};

struct engine_t
{
    struct cat_t* cat;
    struct object_t* objects;
};

void
bytecode_instruction_push(struct bytecode_t*, struct bytecode_instruction_t);

struct bytecode_unit_t
bytecode_execute(const struct engine_t engine, size_t obj_idx, struct bytecode_t);

struct engine_t
engine_make();

void
engine_destroy(struct engine_t);

size_t
engine_memory_needed();

size_t
engine_object_count(struct engine_t engine);

/// inserts object and returns id
size_t engine_object_insert(struct engine_t* engine, struct object_t object);
void engine_object_delete(struct engine_t engine, size_t idx);
void engine_object_set_child(struct engine_t engine, size_t parent_idx, size_t child_idx);
void engine_object_set_attribute(struct engine_t engine, size_t object_idx, struct string_t attribute, struct attribute_value_t value);
void engine_object_bind_attributes(struct engine_t engine, size_t obj1_idx, struct binding_t binding);

void engine_object_set_width(struct engine_t engine, size_t object_idx, uint32_t value);
void engine_object_set_height(struct engine_t engine, size_t object_idx, uint32_t value);
void engine_object_set_x(struct engine_t engine, size_t object_idx, uint32_t value);
void engine_object_set_y(struct engine_t engine, size_t object_idx, uint32_t value);
void engine_object_set_color(struct engine_t engine, size_t object_idx, struct color_t value);
void engine_object_set_text(struct engine_t engine, size_t object_idx, struct string_t value);


int64_t
engine_object_x(const struct engine_t engine, size_t object_idx);
int64_t
engine_object_y(struct engine_t const engine, size_t object_idx);
int64_t
engine_object_width(struct engine_t const engine, size_t object_idx);
int64_t
engine_object_height(struct engine_t const engine, size_t object_idx);
int64_t
engine_object_implicit_width(struct engine_t const engine, size_t object_idx);
int64_t
engine_object_implicit_height(struct engine_t const engine, size_t object_idx);
struct color_t
engine_object_color(const struct engine_t engine, size_t object_idx);
struct string_t
engine_object_text(struct engine_t const engine, size_t object_idx);
struct attribute_value_t
engine_object_attribute(struct engine_t const engine, size_t object_idx, struct string_t key);
struct Texture*
engine_object_texture(struct engine_t const engine, size_t object_idx);

int64_t
engine_get_object_by_id(struct engine_t engine, const char* id);

struct attribute_value_t
attribute_make_i(enum attribute_type type, int64_t value);

struct attribute_value_t
attribute_make_r(enum attribute_type type, double value);

struct attribute_value_t
attribute_make_b(enum attribute_type type, bool value);
