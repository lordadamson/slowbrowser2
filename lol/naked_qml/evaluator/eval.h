#pragma once

#include "../../engine/engine.h"
#include "../parser/parser.h"

extern void error(const char msg[]);

#define max_symbols_count 64

struct evaluated_t
{
    struct object_t obj;
    struct string_t name;
};

struct symbols_t
{
    struct string_t symbols[max_symbols_count];
    size_t symbols_count;
};

struct symbols_t
find_symbols(struct AST ast);

int
eval(struct AST, struct engine_t*, const char * const qml_directory);
