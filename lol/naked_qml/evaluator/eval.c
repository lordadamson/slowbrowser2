#include "eval.h"

#include "../../ui/lib/cat.h"

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>

struct context_t
{
    struct engine_t* engine;
    struct object_t root;
    struct AST ast;
    size_t idx;
    int32_t obj_id;
    const char* const qml_directory;
    struct evaluated_t* evaluated_objects;
    size_t evaluated_objects_count;
};

static _Thread_local jmp_buf jump_buffer;

#define current_object ctx->engine->objects[ctx->obj_id]
#define current_node ctx->ast.nodes[ctx->idx]

static void
panic(const char* const msg)
{
    error(msg);
    abort();
}

static struct ASTNode
peek(struct context_t* ctx)
{
    return ctx->ast.nodes[ctx->idx];
}

static struct ASTNode
peek2(struct context_t* ctx)
{
    return ctx->ast.nodes[ctx->idx+1];
}

static struct ASTNode
peek3(struct context_t* ctx)
{
    return ctx->ast.nodes[ctx->idx+2];
}

static struct ASTNode
at(struct context_t* ctx, size_t i)
{
    return ctx->ast.nodes[i];
}

static struct ASTNode
eat(struct context_t* ctx)
{
    return ctx->ast.nodes[ctx->idx++];
}

static void
eatn(struct context_t* ctx, size_t n)
{
    for(size_t i = 0; i < n; i++)
    {
        eat(ctx);
    }
}

static struct ASTNode
expect(struct context_t* ctx, enum TokenType type)
{
    if(current_node.type != type)
    {
        panic("Unexpected symbol");
    }

    return ctx->ast.nodes[ctx->idx++];
}

static uint8_t
hex_to_decimal(char hex)
{
    uint8_t result = 0;

    if(hex <= '9') // from '0' to '9'
    {
        return hex - 48; // 48 - 48 = 0
    }
    else if(hex >= 'a' && hex <= 'f') // from 'a' to 'f'
    {
        return hex - 87; // 97 - 87 = 10
    }
    else if(hex >= 'A' && hex <= 'F')
    {
        return hex - 55; // 65 - 55 = 10
    }
    else
    {
        abort();
    }
}

static struct color_t
color_from_string3(struct string_t str)
{
    struct color_t color = {0};

    const uint8_t scale = 255 / 16;

    color.r = hex_to_decimal(str.string[0]) * scale;
    color.g = hex_to_decimal(str.string[1]) * scale;
    color.b = hex_to_decimal(str.string[2]) * scale;
    color.a = 255;

    return color;
}

static struct color_t
color_from_string6(struct string_t str)
{
    struct color_t color = {0};

    color.r = 16*hex_to_decimal(str.string[0]) + hex_to_decimal(str.string[1]);
    color.g = 16*hex_to_decimal(str.string[2]) + hex_to_decimal(str.string[3]);
    color.b = 16*hex_to_decimal(str.string[4]) + hex_to_decimal(str.string[5]);
    color.a = 255;

    return color;
}

static struct color_t
color_from_string8(struct string_t str)
{
    struct color_t color = color_from_string6(str);

    color.a = 16*hex_to_decimal(str.string[6]) + hex_to_decimal(str.string[7]);

    return color;
}

static struct color_t
color_from_string(struct string_t str)
{
    assert(str.length == 3 || str.length == 6 || str.length == 8);

    switch(str.length) {
    case 3: return color_from_string3(str);
    case 6: return color_from_string6(str);
    case 8: return color_from_string8(str);
    default: abort();
    }
}

static bool
is_number(enum TokenType t)
{
    return t == TOK_INT || t == TOK_DOUBLE;
}

static bool
is_number_or_identifier(enum TokenType t)
{
    return is_number(t) || is_attribute(t);
}

static bool
is_math_op(enum TokenType t)
{
    return t == TOK_TIMES || t == TOK_PLUS || t == TOK_MINUS || t == TOK_DIVIDE;
}

struct peek_result_t
{
    struct ASTNode n;
    size_t idx;
    size_t how_many_skips;
};

static void
cstring_append(char* destination, const char* const appender)
{
    size_t destination_length = strlen(destination);

    strcpy(destination + destination_length, appender);
}

static struct peek_result_t
concat_identifier(struct context_t* ctx, size_t from)
{
    struct ASTNode result = {0};
    result.type = TOK_LOWERCASE_IDENTIFIER;
    size_t i = from+1;

    for(; i < ctx->ast.count; i += 2)
    {
        if(at(ctx, i-1).type == TOK_DOT && is_attribute(at(ctx, i).type))
        {
            cstring_append(result.value.string, at(ctx, i).value.string);
            cstring_append(result.value.string, ".");
        }
        else if(is_attribute(at(ctx, i-1).type))
        {
            cstring_append(result.value.string, at(ctx, i-1).value.string);
            break;
        }
        else
        {
            abort();
        }
    }

    return (struct peek_result_t){.n = result, .idx = i, .how_many_skips = i - from};
}

static struct peek_result_t
expr_peek_from(struct context_t* ctx, size_t from)
{
    struct ASTNode n = at(ctx, from);

    if(n.type == TOK_DOT)
    {
        return concat_identifier(ctx, from);
    }

    return (struct peek_result_t){.n = n, .idx = from+1, .how_many_skips = 1};
}

struct tokenized_t
{
    struct string_t object_id;
    struct string_t attribute;
};

static int
find(const char* const string, char c)
{
    size_t length = strlen(string);

    for(size_t i = 0; i < length; i++)
    {
        if(string[i] == c)
        {
            return i;
        }
    }

    return -1;
}

static struct tokenized_t
tokenize(const char* const identifier)
{
    struct tokenized_t output = {0};

    int token_idx = find(identifier, '.');

    if(token_idx == -1)
    {
        output.attribute = string_from_char_array(identifier);
        return output;
    }

    output.object_id = string_from_char_array_and_len(identifier, token_idx);
    token_idx++;
    output.attribute = string_from_char_array(identifier + token_idx);

    return output;
}

static struct bytecode_unit_t
attribute_make(struct context_t* ctx, const char* const identifier)
{
    struct bytecode_unit_t output = {0};
    size_t length = 0;
    output.type = BYTECODE_UNIT_ATTRIBUTE;

    struct attribute_reference_t ref = {0};

    if(strcmp(identifier, "Window_Width") == 0)
    {
        ref.type = Attribute_Reference_Window_Width;
        output.value.attribute = ref;
        return output;
    }
    if(strcmp(identifier, "Window_Height") == 0)
    {
        ref.type = Attribute_Reference_Window_Height;
        output.value.attribute = ref;
        return output;
    }

    output.value.attribute.type = Attribute_Reference_Other;

    struct tokenized_t tokens = tokenize(identifier);

    int64_t obj_idx = -1;

    if(tokens.object_id.length == 0)
    {
        obj_idx = ctx->obj_id;
    }
    else
    {
        obj_idx = engine_get_object_by_id(*ctx->engine, tokens.object_id.string);
    }

    if(obj_idx < 0)
    {
        fprintf(stderr, "Couldn't find %s\n", identifier);
        longjmp(jump_buffer, 1);
    }

    struct object_t* obj = &ctx->engine->objects[obj_idx];

    if(strcmp(tokens.attribute.string, "x") == 0)
    {
        output.value.attribute.ref = &obj->x;
    }
    else if(strcmp(tokens.attribute.string, "y") == 0)
    {
        output.value.attribute.ref = &obj->y;
    }
    else if(strcmp(tokens.attribute.string, "width") == 0)
    {
        output.value.attribute.ref = &obj->width;
    }
    else if(strcmp(tokens.attribute.string, "height") == 0)
    {
        output.value.attribute.ref = &obj->height;
    }
    else if(strcmp(tokens.attribute.string, "implicit_width") == 0)
    {
        output.value.attribute.ref = &obj->implicit_width;
    }
    else if(strcmp(tokens.attribute.string, "implicit_height") == 0)
    {
        output.value.attribute.ref = &obj->implicit_height;
    }

    return output;
}


static struct bytecode_unit_t
assign_bytecode_argtype_from_astnode(struct context_t* ctx, struct ASTNode arg)
{
    struct bytecode_unit_t output = {0};
    size_t length = 0;

    if(is_attribute(arg.type))
    {
        return attribute_make(ctx, arg.value.string);
    }

    switch(arg.type)
    {
        case TOK_INT:
            output.type = BYTECODE_UNIT_INT;
            output.value.integer = arg.value.integer;
            return output;
        case TOK_DOUBLE:
            output.type = BYTECODE_UNIT_REAL;
            output.value.real = arg.value.real;
            return output;
        default: abort();
    }
}

static struct bytecode_unit_t
make_pop()
{
    struct bytecode_unit_t output = {
        .type = BYTECODE_UNIT_POP
    };

    return output;
}

static enum InstructionType
ast_to_instruction_type(enum TokenType t)
{
    switch(t)
    {
        case TOK_PLUS: return BYTECODE_ADD;
        case TOK_MINUS: return BYTECODE_SUB;
        case TOK_TIMES: return BYTECODE_MUL;
        case TOK_DIVIDE: return BYTECODE_DIV;
        default: abort();
    }
}

static struct bytecode_instruction_t
instruction_make_pop_pop(struct ASTNode op)
{
    struct bytecode_instruction_t i = {0};

    i.type = ast_to_instruction_type(op.type);
    i.args[0] = make_pop();
    i.args[1] = make_pop();

    return i;
}

static struct bytecode_instruction_t
instruction_make_pop_arg(struct context_t* ctx, struct ASTNode op, struct ASTNode arg)
{
    struct bytecode_instruction_t i = {0};

    i.type = ast_to_instruction_type(op.type);
    i.args[0] = make_pop();
    i.args[1] = assign_bytecode_argtype_from_astnode(ctx, arg);

    return i;
}

static struct bytecode_instruction_t
instruction_make(struct context_t* ctx, struct ASTNode op, struct ASTNode arg1, struct ASTNode arg2)
{
    struct bytecode_instruction_t i = {0};

    i.type = ast_to_instruction_type(op.type);
    i.args[0] = assign_bytecode_argtype_from_astnode(ctx, arg1);
    i.args[1] = assign_bytecode_argtype_from_astnode(ctx, arg2);

    return i;
}

static struct bytecode_instruction_t
instruction_make_push(struct context_t* ctx, struct ASTNode arg)
{
    struct bytecode_instruction_t i = {0};

    i.type = BYTECODE_PUSH;
    i.args[0] = assign_bytecode_argtype_from_astnode(ctx, arg);

    return i;
}

static struct bytecode_t
evaluate_math(struct context_t* ctx)
{
    struct bytecode_t bc = {0};

    expect(ctx, TOK_BEGIN_EXPR);

    struct peek_result_t simple = expr_peek_from(ctx, ctx->idx);
    struct peek_result_t simple2 = expr_peek_from(ctx, simple.idx);

    if(simple2.n.type == TOK_END_EXPR)
    {
        eatn(ctx, simple.how_many_skips);
        eat(ctx);
        struct bytecode_instruction_t i = instruction_make_push(ctx, simple.n);
        bytecode_instruction_push(&bc, i);
        return bc;
    }

    while(current_node.type != TOK_END_EXPR)
    {
        struct peek_result_t first_ = expr_peek_from(ctx, ctx->idx);
        struct peek_result_t second_ = expr_peek_from(ctx, first_.idx);
        struct peek_result_t third_ = expr_peek_from(ctx, second_.idx);

        struct ASTNode first = first_.n;
        struct ASTNode second = second_.n;
        struct ASTNode third = third_.n;

        if(is_number_or_identifier(first.type) && is_number_or_identifier(second.type) && is_math_op(third.type))
        {
            eatn(ctx, first_.how_many_skips);
            eatn(ctx, second_.how_many_skips);
            eatn(ctx, third_.how_many_skips);

            struct bytecode_instruction_t i = instruction_make(ctx, third, second, first);
            bytecode_instruction_push(&bc, i);
        }
        else if(is_number_or_identifier(first.type) && is_number_or_identifier(second.type) && is_number_or_identifier(third.type))
        {
            eatn(ctx, first_.how_many_skips);

            struct bytecode_instruction_t i = instruction_make_push(ctx, first);
            bytecode_instruction_push(&bc, i);
        }
        else if(is_number_or_identifier(first.type) && is_math_op(second.type))
        {
            eatn(ctx, first_.how_many_skips);
            eatn(ctx, second_.how_many_skips);

            struct bytecode_instruction_t i = instruction_make_pop_arg(ctx, second, first);
            bytecode_instruction_push(&bc, i);
        }
        else if(is_math_op(first.type))
        {
            eatn(ctx, first_.how_many_skips);

            struct bytecode_instruction_t i = instruction_make_pop_pop(first);
            bytecode_instruction_push(&bc, i);
        }
        else
        {
            break;
        }
    }

    expect(ctx, TOK_END_EXPR);

    return bc;
}

static struct bytecode_unit_t
evaluate_math_bytecode(struct context_t* ctx)
{
    struct bytecode_t bc = evaluate_math(ctx);

    return bytecode_execute(*ctx->engine, ctx->obj_id, bc);
}

static int64_t
evaluate_int_attribute(struct context_t* ctx)
{
    expect(ctx, TOK_BEGIN_EXPR);

    struct ASTNode first = peek(ctx);
    struct ASTNode second = peek2(ctx);

    if(is_number(first.type) && is_number(second.type))
    {
        struct bytecode_unit_t n = evaluate_math_bytecode(ctx);

        switch (n.type)
        {
            case BYTECODE_UNIT_INT: return n.value.integer;
            case BYTECODE_UNIT_REAL: return (int)n.value.real;
            default: abort();
        }
    }
    else if(is_number(first.type))
    {
        eat(ctx);
        eat(ctx);

        return first.value.integer;
    }
    else
    {
        abort();
    }
}

static bool
is_simple_number(struct context_t* ctx)
{
    struct ASTNode first = peek(ctx);
    struct ASTNode second = peek2(ctx);
    struct ASTNode third = peek3(ctx);

    if(first.type == TOK_BEGIN_EXPR &&
       is_number(second.type) &&
       third.type == TOK_END_EXPR)
    {
        return true;
    }

    return false;
}

static void
handle_int_attribute(struct context_t* ctx, struct attribute_value_t* a)
{
    if(is_simple_number(ctx))
    {
        a->type = Attribute_Integer;
        a->value.integer = evaluate_int_attribute(ctx);
    }
    else
    {
        a->type = Attribute_Bytecode;
        a->value.instructions = evaluate_math(ctx);
    }
}

static void
x(struct context_t* ctx)
{
    handle_int_attribute(ctx, &current_object.x);
}

static void
y(struct context_t* ctx)
{
    handle_int_attribute(ctx, &current_object.y);
}

static void
visible(struct context_t* ctx)
{
    struct ASTNode n = eat(ctx);

    switch(n.type)
    {
        case TOK_TRUE_SYM: current_object.visible = true; break;
        case TOK_FALSE_SYM: current_object.visible = false; break;
        default: panic("Unexpected token");
    }
}

static void
src(struct context_t* ctx)
{
    struct ASTNode n = expect(ctx, TOK_STRING);

    char path[2048] = {0};

    cstring_append(path, ctx->qml_directory);
    cstring_append(path, "/");
    cstring_append(path, n.value.string);

    struct cat_texture_t ct = cat_texture_from_file(path);
    current_object.texture = ct.texture;

    if(current_object.width.type == Attribute_Integer && current_object.width.value.integer == 0)
    {
        current_object.width.type = Attribute_Integer;
        current_object.width.value.integer = ct.width;
    }

    if(current_object.height.type == Attribute_Integer && current_object.height.value.integer == 0)
    {
        current_object.height.type = Attribute_Integer;
        current_object.height.value.integer = ct.height;
    }

    current_object.implicit_width.type = Attribute_Integer;
    current_object.implicit_width.value.integer = ct.width;

    current_object.implicit_height.type = Attribute_Integer;
    current_object.implicit_height.value.integer = ct.height;
}

static void
text(struct context_t* ctx)
{
    struct ASTNode n = expect(ctx, TOK_STRING);

    current_object.text.type = Attribute_String;
    strcpy(current_object.text.value.string.string, n.value.string);
    current_object.text.value.string.length = strlen(n.value.string);
}

static void
width(struct context_t* ctx)
{
    handle_int_attribute(ctx, &current_object.width);
}

static void
height(struct context_t* ctx)
{
    handle_int_attribute(ctx, &current_object.height);
}

static void
implicit_width(struct context_t* ctx)
{
    handle_int_attribute(ctx, &current_object.implicit_width);
}

static void
implicit_height(struct context_t* ctx)
{
    handle_int_attribute(ctx, &current_object.implicit_height);
}

static void
color(struct context_t* ctx)
{
    struct ASTNode n = expect(ctx, TOK_STRING);

    size_t length = strlen(n.value.string);
    assert(length >= 4);
    assert(n.value.string[0] == '#');

    struct string_t s = {0};

    // skipping the #
    // example: #3fa
    strncpy(s.string, n.value.string + 1, length-1);

    s.length = strlen(s.string);
    current_object.color = color_from_string(s);
}

static void
assignment_int(struct context_t* ctx)
{
    expect(ctx, TOK_INT_SYM);

    struct ASTNode key_node = expect(ctx, TOK_LOWERCASE_IDENTIFIER);
    struct attribute_value_t value = {0};

    switch (current_node.type)
    {
    case TOK_LOWERCASE_IDENTIFIER:
    case TOK_UPPERCASE_IDENTIFIER:
        value.type = Attribute_Bytecode;
        value.value.instructions = (struct bytecode_t){0};
        break;
    case TOK_INT:
        value.type = Attribute_Integer;
        value.value.integer = current_node.value.integer;
        break;
    case TOK_DOUBLE:
        value.type = Attribute_Real;
        value.value.real = current_node.value.real;
        break;
    default:
        panic("we done goofed");
    }

    struct string_t key = string_from_char_array(key_node.value.string);

    engine_object_set_attribute(*ctx->engine, ctx->obj_id, key, value);
}

static void
assignment(struct context_t* ctx)
{

}

static void
object_type(struct context_t* ctx)
{
    struct ASTNode node = expect(ctx, TOK_UPPERCASE_IDENTIFIER);

    if(strcmp(node.value.string, "Rectangle") == 0)
    {
        current_object.type = Object_Rectangle;
    }
    else if(strcmp(node.value.string, "Item") == 0)
    {
        current_object.type = Object_Item;
    }
    else if(strcmp(node.value.string, "Oval") == 0)
    {
        current_object.type = Object_Oval;
    }
    else if(strcmp(node.value.string, "Text") == 0)
    {
        current_object.type = Object_Text;
    }
    else if(strcmp(node.value.string, "Image") == 0)
    {
        current_object.type = Object_Image;
    }
    else if(strcmp(node.value.string, "MouseArea") == 0)
    {
        current_object.type = Object_MouseArea;
    }
    else if(strcmp(node.value.string, "Row") == 0)
    {
        current_object.type = Object_Row;
    }
    else if(strcmp(node.value.string, "Column") == 0)
    {
        current_object.type = Object_Column;
    }
    else
    {
        bool found = false;

        for(size_t i = 0; i < ctx->evaluated_objects_count; i++)
        {
            if(strcmp(node.value.string, ctx->evaluated_objects[i].name.string) == 0)
            {
                found = true;
                current_object =  ctx->evaluated_objects[i].obj;
                break;
            }
        }

        if(!found)
        {
            abort();
        }
    }
}

static void
id(struct context_t* ctx)
{
    strcpy(current_object.id.string, current_node.value.string);
    current_object.id.length = strlen(current_node.value.string);
    eat(ctx);
}

static struct object_t
get_parent(struct context_t* ctx)
{
    return ctx->engine->objects[current_object.parent];
}

static struct bytecode_instruction_t
instruction_push_attribute(struct context_t* ctx, const char* const identifier)
{
    struct bytecode_instruction_t i = {0};
    i.type = BYTECODE_PUSH;

    struct bytecode_unit_t output = attribute_make(ctx, identifier);
    i.args[0] = output;

    return i;
}

static struct bytecode_instruction_t
window_width_instruction(struct context_t* ctx)
{
    return instruction_push_attribute(ctx, "Window_Width");
}

static struct bytecode_instruction_t
window_height_instruction(struct context_t* ctx)
{
    return instruction_push_attribute(ctx, "Window_Height");
}

static struct attribute_value_t
window_width_make(struct context_t* ctx)
{
    struct attribute_value_t i = {0};
    i.type = Attribute_Bytecode;
    i.value.instructions.instructions[0] = window_width_instruction(ctx);
    i.value.instructions.count = 1;
    return i;
}

static struct attribute_value_t
window_height_make(struct context_t* ctx)
{
    struct attribute_value_t i = {0};
    i.type = Attribute_Bytecode;
    i.value.instructions.instructions[0] = window_height_instruction(ctx);
    i.value.instructions.count = 1;
    return i;
}

static void
object_default_values(struct context_t* ctx)
{
    current_object.color = (struct color_t){ 0, 0, 0, 255 };

    if(current_object.parent == -1)
    {
        current_object.visible = true;
        current_object.x.type = Attribute_Integer;
        current_object.x.value.integer = 0;
        current_object.y.type = Attribute_Integer;
        current_object.y.value.integer = 0;
        current_object.width = window_width_make(ctx);
        current_object.height = window_height_make(ctx);
        current_object.implicit_width = current_object.width;
        current_object.implicit_height = current_object.height;
    }
    else
    {
        struct object_t parent = get_parent(ctx);

        current_object.x = parent.x;
        current_object.y = parent.y;
        current_object.width = parent.width;
        current_object.height = parent.height;
        current_object.visible = parent.visible;
        current_object.implicit_width = parent.implicit_width;
        current_object.implicit_height = parent.implicit_height;
    }

}

static void
object_second_pass(struct context_t* ctx)
{
    struct object_t obj = {0};

    int32_t parent = ctx->obj_id;
    ctx->obj_id++;
    current_object.parent = parent;

    expect(ctx, TOK_UPPERCASE_IDENTIFIER);

    while(ctx->idx < ctx->ast.count)
    {
        struct ASTNode n = peek(ctx);

        switch(n.type)
        {
        case TOK_ELEMENT:
            eat(ctx);
            int32_t tmp_id = ctx->obj_id;
            object_second_pass(ctx);
            ctx->obj_id = tmp_id;
            break;
        case TOK_ID_SYM:
            eat(ctx);
            break;
        case TOK_ASSIGNMENT:
            eat(ctx);
            assignment(ctx);
            break;
        case TOK_INT_SYM:
            eat(ctx);
            assignment_int(ctx);
            break;
        case TOK_VISIBLE_SYM:
            eat(ctx);
            visible(ctx);
            break;
        case TOK_SRC_SYM:
            eat(ctx);
            src(ctx);
            break;
        case TOK_TEXT_SYM:
            eat(ctx);
            text(ctx);
            break;
        case TOK_WIDTH_SYM:
            eat(ctx);
            width(ctx);
            break;
        case TOK_HEIGHT_SYM:
            eat(ctx);
            height(ctx);
            break;
        case TOK_IMPLICIT_WIDTH_SYM:
            eat(ctx);
            implicit_width(ctx);
            break;
        case TOK_IMPLICIT_HEIGHT_SYM:
            eat(ctx);
            implicit_height(ctx);
            break;
        case TOK_X_SYM:
            eat(ctx);
            x(ctx);
            break;
        case TOK_Y_SYM:
            eat(ctx);
            y(ctx);
            break;
        case TOK_COLOR_SYM:
            eat(ctx);
            color(ctx);
            break;
        default:
            panic("Unexpected ASTNodeType");
        }
    }
}

static void
object_first_pass(struct context_t* ctx)
{
    struct object_t obj = {0};

    int32_t parent = ctx->obj_id;
    ctx->obj_id = engine_object_insert(ctx->engine, obj);
    current_object.parent = parent;

    object_type(ctx);
    object_default_values(ctx);

    while(ctx->idx < ctx->ast.count)
    {
        struct ASTNode n = peek(ctx);

        switch(n.type)
        {
        case TOK_ELEMENT:
            eat(ctx);
            int32_t tmp_id = ctx->obj_id;
            object_first_pass(ctx);
            ctx->obj_id = tmp_id;
            break;
        case TOK_ID_SYM:
            id(ctx);
            break;
        default:
            eat(ctx);
            break;
        }
    }
}

/// This pass should only create the objects and their IDs
/// It will not parse any of the attributes (except ids)
static void
first_pass(struct context_t* ctx)
{
    expect(ctx, TOK_ELEMENT);
    object_first_pass(ctx);
}

/// This pass will fill in all the attributes of the different objects
static void
second_pass(struct context_t* ctx)
{
    expect(ctx, TOK_ELEMENT);
    object_second_pass(ctx);
}

struct context_t
context_initialize(struct AST ast, struct engine_t* engine, const char* const qml_directory)
{
    struct context_t ctx = {
        .engine = engine,
        .ast = ast,
        .idx = 0,
        .qml_directory = qml_directory,
        .obj_id = -1
    };

    return ctx;
}

struct context_t
context_initialize2(struct AST ast, struct object_t root, const char* const qml_directory)
{
    struct context_t ctx = {
        .root = root,
        .ast = ast,
        .idx = 0,
        .qml_directory = qml_directory,
        .obj_id = -1
    };

    return ctx;
}

int
eval(struct AST ast, struct engine_t* engine, const char* const qml_directory)
{
    if(setjmp(jump_buffer) != 0)
    {
        return -1;
    }

    struct context_t ctx_first_pass = context_initialize(ast, engine, qml_directory);

    first_pass(&ctx_first_pass);

    struct context_t ctx_second_pass = context_initialize(ast, engine, qml_directory);

    second_pass(&ctx_second_pass);

    return 0;
}

int
eval2(struct AST ast, struct object_t root, const char* const qml_directory)
{
    if(setjmp(jump_buffer) != 0)
    {
        return -1;
    }

    struct context_t ctx_first_pass = context_initialize2(ast, root, qml_directory);

    first_pass(&ctx_first_pass);

    struct context_t ctx_second_pass = context_initialize2(ast, root, qml_directory);

    second_pass(&ctx_second_pass);

    return 0;
}

struct symbols_t
find_symbols(struct AST ast)
{

}

