#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "lexer.h"
#include "../tokens/tokens.h"

#include "../../common/stb_ds.h"

struct lexer_t
{
	const char* file_content;
	size_t file_size;
	size_t pos;
	size_t line;
	size_t col;
    struct Token* output;
};

static struct lexer_t* lexers;
static int full_lexers[MAX_LEXERS];
static int lexers_count = 0;

#define error_msg_size 1024
static _Thread_local char error_msg[error_msg_size];

#ifdef ctx
#undef ctx
#endif

#define ctx lexers[lx]

const char*
here_ptr(lexer_handle lx)
{
    return ctx.file_content + ctx.pos;
}

static char
here(lexer_handle lx)
{
    return ctx.file_content[ctx.pos];
}

static char
at(lexer_handle lx, size_t idx)
{
    return ctx.file_content[idx];
}

struct Token
token_make(lexer_handle lx, int type, size_t length)
{
    struct Token t = {
        .type = type,
        .lexeme = here_ptr(lx),
        .lexeme_size = length,
        .line = ctx.line,
        .column = ctx.col
    };

    return t;
}

static struct Token
map(lexer_handle lx, const char* symbol)
{
    enum TokenType t = string_to_tokentype(symbol);
    return token_make(lx, t, strlen(symbol));
}

static int
find_empty_lexer()
{
    for(size_t i = 0; i < MAX_LEXERS; i++)
    {
        if(full_lexers[i] == 0)
        {
            return i;
        }
    }

    abort();
}

lexer_handle
lexer_init(const char* file_content, struct memory_block_t memory)
{
    if(lexers == NULL)
    {
        assert(memory.size >= lexer_memory_needed());
        lexers = memory.mem;
    }

	struct lexer_t lexer = {
		.file_content = file_content,
		.file_size = strlen(file_content),
		.line = 1
	};

    size_t lexer_index = lexers_count;

    if(lexers_count >= MAX_LEXERS)
    {
        lexer_index = find_empty_lexer();
    }
    else
    {
        lexers_count++;
    }

    lexers[lexer_index] = lexer;

    return lexer_index;
}

void
lexer_reset(lexer_handle lx)
{
    memset(&ctx, 0, sizeof(ctx));
}

static int
accept(lexer_handle lx, const char* what)
{
	size_t what_size = strlen(what);

    if(ctx.pos + what_size > ctx.file_size)
	{
		return 0;
	}

	return strncmp(here_ptr(lx), what, what_size) == 0;
}

static void
advance(lexer_handle lx, size_t how_much)
{
	ctx.pos += how_much;
	ctx.col += how_much;
}

static int
is_space(char c)
{
    return c == ' ' || c == '\t' || c == '\v' || c == '\f' || c == '\r';
}

static int
is_newline(char c)
{
    return c == '\n';
}

static void
handle_newline(lexer_handle lx)
{
	advance(lx, 1);
	ctx.line++;
	ctx.col = 0;
}

static void
skip_until(lexer_handle lx, char until)
{
	while(ctx.pos < ctx.file_size)
	{
		if(here(lx) == until)
		{
			return;
		}

		if(is_newline(here(lx)))
		{
			handle_newline(lx);
			continue;
		}

		advance(lx, 1);
	}
}

static void
skip_until_s(lexer_handle lx, const char* until)
{
	while(ctx.pos < ctx.file_size)
	{
		if(accept(lx, until))
		{
            advance(lx, strlen(until));
			return;
		}

		if(is_newline(here(lx)))
		{
			handle_newline(lx);
			continue;
		}

		advance(lx, 1);
	}
}

static void
skip(lexer_handle lx)
{
	while(ctx.pos < ctx.file_size)
	{
		if(is_space(here(lx)))
		{
			advance(lx, 1);
			continue;
		}

		if(is_newline(here(lx)))
		{
			handle_newline(lx);
			continue;
		}

		if(accept(lx, "//"))
		{
			skip_until(lx, '\n');
			ctx.line++;
			ctx.col = 0;
			continue;
		}

		if(accept(lx, "/*"))
		{
            skip_until_s(lx, "*/");
			continue;
		}

		return;
	}
}

struct Token
advance_n_return_token(lexer_handle lx, int kind, size_t length)
{
    struct Token t = token_make(lx, kind, length);

	advance(lx, length);

	return t;
}

static bool
is_uppercase(char c)
{
    return c >= 'A' && c <= 'Z';
}

static bool
is_alpha(char c)
{
    return c >= 'A' && c <= 'z';
}

static bool
is_num(char c)
{
    return c >= '0' && c <= '9';
}

static bool
is_alphanum(char c)
{
    return is_alpha(c) || is_num(c);
}

static int
alphanum_length(const char* str, size_t str_length)
{
    size_t i = 0;

    for(; i < str_length; i++)
    {
        if(is_space(str[i]))
        {
            return i;
        }

        if(is_newline(str[i]))
        {
            return i;
        }

        if(is_alphanum(str[i]) == 0)
        {
            return i;
        }
    }

    return i;
}

static int
is_identifier(lexer_handle lx)
{
    if(is_alpha(here(lx)) == 0)
    {
        return 0;
    }

    return alphanum_length(ctx.file_content + ctx.pos, ctx.file_size - ctx.pos);
}

static int
is_uppercase_identifier(lexer_handle lx)
{
    if(is_uppercase(here(lx)) == 0)
    {
        return 0;
    }

    return alphanum_length(ctx.file_content + ctx.pos, ctx.file_size - ctx.pos);
}

static bool
is_math_op(char c)
{
    return c == '+' || c == '-' || c == '*' || c == '/';
}

static int
is_number(lexer_handle lx)
{
    size_t length = 0;
    bool after_floating_point = false;

    size_t i = ctx.pos;

    for(; i < ctx.file_size; i++, length++)
    {
        if(is_space(at(lx, i)))
        {
            return length;
        }

        if(is_newline(at(lx, i)))
        {
            return length;
        }

        if(at(lx, i) == ')')
        {
            return length;
        }

        if(is_math_op(at(lx, i)))
        {
            return length;
        }

        if(at(lx, i) != '.' && is_num(at(lx, i)) == false)
        {
            return 0;
        }

        if(at(lx, i) == '.' && after_floating_point)
        {
            return 0;
        }

        if(at(lx, i) == '.')
        {
            after_floating_point = true;
            continue;
        }
    }

    return length;
}

static int
is_string(lexer_handle lx)
{
    if(here(lx) != '"')
    {
        return 0;
    }

    size_t length = 1;
    for(size_t i = ctx.pos+1; i < ctx.file_size; i++, length++)
    {
        if(at(lx, i) == '"')
        {
            return length + 1;
        }

        if(at(lx, i) == '\n' && at(lx, i-1) == '\\')
        {
            continue;
        }

        if(at(lx, i) == '\n')
        {
            return 0;
        }
    }

    return 0;
}

static int
unknown_token_length(lexer_handle lx)
{
    size_t length = 0;

    for(size_t i = ctx.pos; i < ctx.file_size; i++, length++)
    {
        if(is_space(at(lx, i)))
        {
            return length;
        }

        if(is_newline(at(lx, i)))
        {
            return length;
        }
    }

    return length;
}

struct Token
lex(lexer_handle lx)
{
	while(ctx.pos < ctx.file_size)
	{
		skip(lx);

        if(ctx.pos >= ctx.file_size)
        {
            break;
        }

		for(size_t i = 0; i < symbols_count; i++)
		{
			if(accept(lx, symbols[i]))
			{
                struct Token t = map(lx, symbols[i]);
                advance(lx, strlen(symbols[i]));
                return t;
			}
		}

        size_t length = is_number(lx);

        if(length)
        {
            return advance_n_return_token(lx, TOK_NUMBER, length);
        }

		length = is_uppercase_identifier(lx);

		if(length)
		{
            return advance_n_return_token(lx, TOK_UPPERCASE_IDENTIFIER, length);
		}

        length = is_identifier(lx);

		if(length)
		{
            return advance_n_return_token(lx, TOK_LOWERCASE_IDENTIFIER, length);
        }

		length = is_string(lx);

		if(length)
		{
            return advance_n_return_token(lx, TOK_STRING, length);
		}

		length = unknown_token_length(lx);
        return advance_n_return_token(lx, TOK_UNKNOWN, length);
	}

    struct Token t = {
        .type = TOK_NONE,
        .lexeme = here_ptr(lx),
        .lexeme_size = 0,
        .eof = 1,
        .line = ctx.line,
        .column = ctx.col
	};

	return t;
}

size_t
lexer_memory_needed()
{
    return sizeof(struct lexer_t) * MAX_LEXERS;
}


