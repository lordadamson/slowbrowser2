#pragma once

#include <stddef.h>

#include "../tokens/tokens.h"
#include "../../common/mem.h"

// the library expects these two functions to be defined
extern void error(const char msg[]);

/// that's how many lexers you can have at any given moment
/// if you need more, increase this number
#define MAX_LEXERS 64

typedef int lexer_handle;

size_t
lexer_memory_needed();

/// Initialization is not thread safe
/// if we fail to initialize a lexer we return -1
/// if we fail to initialize a lexer it will be because
/// \ref MAX_LEXERS is not enough, increase its value, recompile
/// and try again :D
lexer_handle
lexer_init(const char* file_content, struct memory_block_t memory);

void
lexer_reset(lexer_handle);

struct Token
lex(lexer_handle ctx);
