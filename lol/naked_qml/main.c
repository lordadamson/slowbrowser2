#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "lexer/lexer.h"
#include "parser/parser.h"
#include "evaluator/eval.h"
#include "../common/mem.h"
#include "../ui/lib/cat.h"

#define megabyte (1 << 20)

void error(const char msg[])
{
    fprintf(stderr, "%s\n", msg);
    abort();
}

struct ParserToken
nextsym(void * user_data)
{
    lexer_handle lx = *(lexer_handle*)user_data;

    struct LexerToken t = lex(lx);
    char buf[1024] = {0};
    snprintf(buf, t.lexeme_size+1, "%s", t.lexeme);
    printf("%s\n", buf);
    fflush(stdout);

    struct ParserToken pt = {
        .type = t.kind,
        .lexeme = t.lexeme,
        .lexeme_size = t.lexeme_size,
        .column = t.column,
        .line = t.line
    };

    return pt;
}

const char*
file_read(const char* const path)
{
    struct stat buf;
    int exists = (stat(path, &buf) == 0);

    if(!(S_ISREG(buf.st_mode) && exists))
    {
        return NULL;
    }

    if(buf.st_size < 0)
    {
        return NULL;
    }

    if(buf.st_size == 0)
    {
        return malloc(0);
    }

    int fd = open(path, O_RDONLY);

    if(fd < 0)
    {
        return NULL;
    }

    char* output = malloc(buf.st_size + 1);

    ssize_t bytes_read = read(fd, output, buf.st_size);

    output[buf.st_size] = 0;

    close(fd);

    if(bytes_read != buf.st_size)
    {
        free(output);
        return NULL;
    }

    return output;
}

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        fprintf(stderr, "provide a lol file\n");
        abort();
    }
    
    const char* const lol_content = file_read(argv[1]);

    size_t lexer_needs = lexer_memory_needed();
    size_t parser_needs = parser_memory_needed(1);
    size_t engine_needs = engine_memory_needed();

    size_t memory_needed = lexer_needs + parser_needs + engine_needs;

    memory_handle m = lol_memory_init(memory_needed);

    struct memory_block_t lexer_memory = lol_alloc(m, lexer_needs);
    struct memory_block_t parser_memory = lol_alloc(m, parser_needs);
    struct memory_block_t engine_memory = lol_alloc(m, engine_needs);

    lexer_handle lx = lexer_init(lol_content, lexer_memory);

    //struct LexerToken* tokens = lex_gamed(lol_content);

    if(lx == -1)
    {
        fprintf(stderr, "lexer initialization failed\n");
        abort();
    }

    parser_handle p = parser_init(&lx, parser_memory, 1);

    struct AST ast = lol_parse(p);

    for(size_t i = 0; i < ast.count; i++)
    {
        printf("%s ", ast_node_type_to_string(ast.nodes[i].type));

        switch(ast.nodes[i].type)
        {
            case AST_LOWERCASE_IDENTIFIER:
            case AST_UPPERCASE_IDENTIFIER:
            case AST_STRING:
            case AST_INT_SYM:
            case AST_REAL_SYM:
            case AST_BOOL_SYM:
            case AST_STRING_SYM:
                printf("%s", ast.nodes[i].value.string);
                break;
            case AST_DOUBLE:
                printf("%f", ast.nodes[i].value.real);
                break;
            case AST_INT:
                printf("%" PRIi64, ast.nodes[i].value.integer);
                break;
            default:
                break;
        }

        printf("\n");
    }

    fflush(stdout);

    struct engine_t e = {0};
    eval(ast, &e);

    struct cat_t* cat = cat_init(&e);

    while(!cat_should_close(cat))
    {
        cat_update(cat);
    }

    cat_free(cat);
    lol_memory_deinit(m);
    free((void*)lol_content);
}
