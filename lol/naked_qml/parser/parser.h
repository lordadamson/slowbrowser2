#pragma once

#include <stddef.h>
#include <stdint.h>
#include "../tokens/tokens.h"
#include "../../common/mem.h"

#define PARSER_MAX_NODES 1024
#define PARSER_MAX_STRING_LENGTH 1024

/// that's how many parsers you can have at any given moment
/// if you need more, increase this number
#define MAX_PARSERS 64

// the library expects these two functions to be defined
extern void error(const char msg[]);
struct Token nextsym(void*);

typedef int parser_handle;

/// Element -> Uppercase_Identifier '{' Body '}'
/// Body -> BodyMember*
/// BodyMember -> Assignment | Element
/// Assignment -> LowercaseIdentifier ':' MathExpression | LowercaseIdentifier ':' StringExpression
/// MathExpression ->  Term | Term AddOp Term
/// Term -> Factor | Factor MulOp Factor
/// Factor -> Number | Identifier | '(' MathExpression ')'
/// AddOp -> + | -
/// MulOp -> * | /
/// StringExpression -> String | Identifier | String '+' StringExpression | Identifier '+' StringExpression
/// Identifier -> LowercaseIdentifier | Uppercase_Identifier | LowercaseIdentifier.Identifier | Uppercase_Identifier.Identifier
/// LowercaseIdentifier -> [A-z][A-z0-9]*
/// Uppercase_Identifier -> [A-Z][A-z0-9]*
/// String -> '"' .* '"'
struct ASTNode
{
    enum TokenType type;

    union ASTNodeValue {
        int64_t integer;
        double real;
        char string[PARSER_MAX_STRING_LENGTH];
    } value;
};

struct AST
{
    struct ASTNode nodes[PARSER_MAX_NODES];
    size_t count;
    int error;
};

size_t
parser_memory_needed(size_t max_parsers_needed);

/// Initialization is not thread safe
/// if we fail to initialize a parser we return -1
/// if we fail to initialize a parser it will be because
/// \ref MAX_PARSERS is not enough, increase its value, recompile
/// and try again :D
parser_handle
parser_init(void* user_data, struct memory_block_t memory, size_t max_parsers_needed);

void
parser_reset(parser_handle);

struct AST
lol_parse(parser_handle);
