#include "parser.h"

#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <setjmp.h>
#include <stdbool.h>

#include "../../common/stb_ds.h"

#define error_msg_size 1024
static _Thread_local char error_msg[error_msg_size];
static _Thread_local jmp_buf jump_buffer;

#define unexpected_token(token) unexpected_token_error((token).lexeme, (token).lexeme_size, (token).line, (token).column)
#define unexpected_token_msg(token, msg) unexpected_token_error_msg((token).lexeme, (token).lexeme_size, (token).line, (token).column, (msg))

static void
unexpected_token_error_msg(const char* lexeme, size_t lexeme_size, size_t line, size_t column, const char* msg)
{
    snprintf(error_msg, error_msg_size, "at line: %zu, column: %zu, unexpected token: %.*s %s\n", line, column, (int)lexeme_size, lexeme, msg);

    error(error_msg);

    longjmp(jump_buffer, 1);
}

static void
unexpected_token_error(const char* lexeme, size_t lexeme_size, size_t line, size_t column)
{
    unexpected_token_error_msg(lexeme, lexeme_size, line, column, "");
}

static struct ASTNode
ast_make_node_lexeme(const char* identifier, size_t length, enum TokenType t)
{
    struct ASTNode node = {0};
    node.type = t;
    strncpy(node.value.string, identifier, length);
    return node;
}

static struct ASTNode
ast_make_node_uppercase_identifier(const char* identifier, size_t length)
{
    return ast_make_node_lexeme(identifier, length, TOK_UPPERCASE_IDENTIFIER);
}

static struct ASTNode
ast_make_node_lowercase_identifier(const char* identifier, size_t length)
{
    return ast_make_node_lexeme(identifier, length, TOK_LOWERCASE_IDENTIFIER);
}

static struct ASTNode
ast_make_node_string(const char* identifier, size_t length)
{
    assert(length > 0);
    // +1 and -1 to skip the quotation marks
    return ast_make_node_lexeme(identifier + 1, length - 2, TOK_STRING);
}

static struct ASTNode
ast_make_node_number_i(int64_t i)
{
    struct ASTNode node = {0};
    node.type = TOK_INT;
    node.value.integer = i;
    return node;
}

static struct ASTNode
ast_make_node_number_d(double d)
{
    struct ASTNode node = {0};
    node.type = TOK_DOUBLE;
    node.value.real = d;
    return node;
}

static struct ASTNode
ast_make_node(enum TokenType type)
{
    struct ASTNode node = {0};
    node.type = type;
    return node;
}
static struct ASTNode
ast_make_node_op(char op)
{
    struct ASTNode node = {0};

    switch(op) {
    case '+':
        node.type = TOK_PLUS;
        break;
    case '-':
        node.type = TOK_MINUS;
        break;
    case '*':
        node.type = TOK_TIMES;
        break;
    case '/':
        node.type = TOK_DIVIDE;
        break;
    default:
        abort();
    }

    return node;
}

static void
ast_push(struct AST* ast, struct ASTNode node)
{
    ast->nodes[ast->count++] = node;
}

static void
ast_swap(struct AST* ast, size_t first, size_t second)
{
    struct ASTNode tmp = ast->nodes[first];
    ast->nodes[first] = ast->nodes[second];
    ast->nodes[second] = tmp;
}

static void
ast_insert_at(struct AST* ast, struct ASTNode node, size_t at)
{
    ast->count++;
    for(; at < ast->count; at++)
    {
        struct ASTNode tmp = ast->nodes[at];
        ast->nodes[at] = node;
        node = tmp;
    }
}

static void
ast_reverse_from_to(struct AST* ast, size_t from, size_t to)
{
    assert(from <= to);

    while(from < to)
    {
        ast_swap(ast, from++, to--);
    }
}

static void
ast_reverse_from(struct AST* ast, size_t from)
{
    size_t to = ast->count - 1;

    ast_reverse_from_to(ast, from, to);
}

static void
ast_fix_identifiers(struct AST* ast, size_t from)
{
    size_t limit = ast->count-1;
    size_t to = 0;

    for(; from < limit; from++)
    {
        for(size_t i = from; i < ast->count; i++)
        {
            if(is_attribute(ast->nodes[i].type))
            {
                from = i;
                break;
            }
        }

        bool dot_encountered = false;

        for(size_t i = from; i < ast->count; i++)
        {
            if(ast->nodes[i].type == TOK_DOT)
            {
                dot_encountered = true;
            }

            if(is_attribute(ast->nodes[i].type) || ast->nodes[i].type == TOK_DOT)
            {
                to = i;
            }
            else
            {
                break;
            }
        }

        if(dot_encountered == false)
        {
            continue;
        }

        if(to <= from)
        {
            continue;
        }

        ast_reverse_from_to(ast, from, to);
        from = to;
    }
}

struct parser_t
{
    void* user_data;
    struct Token token;
    struct Token real_token;
    struct AST ast;
    struct Token fake_tokens[2];
    size_t fake_tokens_idx;
    size_t fake_tokens_count;
    bool finished_using_fake_tokens;
};

static struct parser_t* parsers;
static int full_parsers[MAX_PARSERS];
static int parsers_count = 0;

#ifdef ctx
#undef ctx
#endif

#define ctx parsers[p]

size_t
parser_memory_needed(size_t max_parsers_needed)
{
    return sizeof(struct parser_t) * max_parsers_needed;
}

static void
_nextsym(parser_handle p)
{
    // struct Token t = ctx.tokens[ctx.tokens_idx++];
    // char buf[1024] = {0};
    // snprintf(buf, t.lexeme_size+1, "%s", t.lexeme);
    // printf("%s\n", buf);
    // fflush(stdout);

    // struct Token pt = {
    //     .type = t.kind,
    //     .lexeme = t.lexeme,
    //     .lexeme_size = t.lexeme_size,
    //     .column = t.column,
    //     .line = t.line
    // };

    // ctx.token = pt;

    if(ctx.finished_using_fake_tokens)
    {
        ctx.token = ctx.real_token;
        ctx.finished_using_fake_tokens = false;
        ctx.fake_tokens_idx = 0;
        ctx.fake_tokens_count = 0;
        memset(ctx.fake_tokens, 0, sizeof(ctx.fake_tokens));
        return;
    }

    if(ctx.fake_tokens_count > 0)
    {
        ctx.token = ctx.fake_tokens[ctx.fake_tokens_idx++];
        ctx.fake_tokens_count--;

        if(ctx.fake_tokens_count == 0)
        {
            ctx.finished_using_fake_tokens = true;
        }

        return;
    }

    ctx.token = nextsym(ctx.user_data);
}

static int
find_empty_parser()
{
    for(size_t i = 0; i < MAX_PARSERS; i++)
    {
        if(full_parsers[i] == 0)
        {
            return i;
        }
    }

    abort();
}

parser_handle
parser_init(void* user_data, struct memory_block_t memory, size_t how_many)
{
    if(parsers == NULL)
    {
        assert(memory.size >= parser_memory_needed(how_many));
        parsers = memory.mem;
    }

    struct parser_t parser = {
        .user_data = user_data
    };

    size_t parser_index = parsers_count;

    if(parsers_count >= MAX_PARSERS)
    {
        parser_index = find_empty_parser();
    }
    else
    {
        parsers_count++;
    }

    parsers[parser_index] = parser;

    return parser_index;
}

void
parser_reset(parser_handle p)
{
    memset(&ctx, 0, sizeof(ctx));
}

static int
accept(parser_handle p, enum TokenType s)
{
    if (ctx.token.type == s)
    {
        _nextsym(p);
        return 1;
    }

    return 0;
}

static int
expect(parser_handle p, enum TokenType s)
{
    if (accept(p, s))
    {
        return 1;
    }

    unexpected_token(ctx.token);
    return 0;
}

static struct Token
accept_t(parser_handle p, enum TokenType s)
{
    struct Token token = {0};

    if (ctx.token.type == s)
    {
        token = ctx.token;
        _nextsym(p);
        return token;
    }

    return token;
}

static struct Token
eat(parser_handle p)
{
    struct Token token = ctx.token;
    _nextsym(p);
    return token;
}

static struct Token
peek(parser_handle p)
{
    // if(ctx.finished_using_fake_tokens)
    // {
    //     return ctx.real_token;
    // }

    return ctx.token;
}

static struct Token
expect_t(parser_handle p, enum TokenType s)
{
    struct Token token = eat(p);

    if(token.type != s)
    {
        unexpected_token(ctx.token);
    }

    return token;
}

static int
is_addop(enum TokenType type)
{
    return type == TOK_PLUS || type == TOK_MINUS;
}

static int
is_mullop(enum TokenType type)
{
    return type == TOK_TIMES || type == TOK_DIVIDE;
}

static int
is_plus(parser_handle p)
{
    return peek(p).type == TOK_PLUS;
}

static int
has_dot(const char* str, size_t length)
{
    for(size_t i = 0; i < length; i++)
    {
        if(str[i] == '.')
        {
            return 1;
        }
    }

    return 0;
}

static void
number(parser_handle p)
{
    struct Token t = expect_t(p, TOK_NUMBER);

    errno = 0;
    char null_terminated[16] = {0};
    char* endptr = NULL;
    strncpy(null_terminated, t.lexeme, t.lexeme_size);

    if(has_dot(t.lexeme, t.lexeme_size))
    {
        double number = strtod(null_terminated, &endptr);

        if(errno != 0)
        {
            unexpected_token(t);
        }

        if(endptr == null_terminated)
        {
            unexpected_token_msg(t, "No digits were found");
        }

        ast_push(&ctx.ast, ast_make_node_number_d(number));
    }
    else
    {
        int64_t number = strtol(null_terminated, &endptr, 10);

        if(errno != 0)
        {
            unexpected_token(t);
        }

        if(endptr == null_terminated)
        {
            unexpected_token_msg(t, "No digits were found");
        }

        ast_push(&ctx.ast, ast_make_node_number_i(number));
    }
}

static void
string(parser_handle p)
{
    struct Token t = expect_t(p, TOK_STRING);
    ast_push(&ctx.ast, ast_make_node_string(t.lexeme, t.lexeme_size));
}

/// Identifier -> LowercaseIdentifier | Uppercase_Identifier | LowercaseIdentifier.Identifier | Uppercase_Identifier.Identifier
static void
identifier(parser_handle p)
{
    struct Token t = eat(p);
    enum TokenType token_type = TOK_NONE;

    const char* lexeme;
    size_t lexeme_size = 0;

    switch(t.type)
    {
        case TOK_ID_SYM:
        case TOK_WIDTH_SYM:
        case TOK_HEIGHT_SYM:
        case TOK_IMPLICIT_WIDTH_SYM:
        case TOK_IMPLICIT_HEIGHT_SYM:
        case TOK_X_SYM:
        case TOK_Y_SYM:
        case TOK_COLOR_SYM:
        case TOK_VISIBLE_SYM:
        case TOK_SRC_SYM:
        case TOK_TEXT_SYM:
            token_type = t.type;
            lexeme = tokentype_to_string(t.type);
            lexeme_size = strlen(lexeme);
            break;
        case TOK_UPPERCASE_IDENTIFIER:
            token_type = TOK_UPPERCASE_IDENTIFIER;
            lexeme = t.lexeme;
            lexeme_size = t.lexeme_size;
            break;
        case TOK_LOWERCASE_IDENTIFIER:
            token_type = TOK_LOWERCASE_IDENTIFIER;
            lexeme = t.lexeme;
            lexeme_size = t.lexeme_size;
            break;
        default: unexpected_token(t);
    }


    if(accept(p, TOK_DOT))
    {
        ast_push(&ctx.ast, ast_make_node(TOK_DOT));
        ast_push(&ctx.ast, ast_make_node_lexeme(lexeme, lexeme_size, token_type));
        identifier(p);
        return;
    }

    ast_push(&ctx.ast, ast_make_node_lexeme(t.lexeme, t.lexeme_size, token_type));
}

static void
math_expression(parser_handle p);

/// Factor -> Number | Identifier | '(' MathExpression ')'
static void
factor(parser_handle p)
{
    struct Token t = peek(p);

    switch(t.type)
    {
        case TOK_UPPERCASE_IDENTIFIER:
        case TOK_LOWERCASE_IDENTIFIER:
        case TOK_X_SYM:
        case TOK_Y_SYM:
        case TOK_WIDTH_SYM:
        case TOK_HEIGHT_SYM:
        case TOK_IMPLICIT_WIDTH_SYM:
        case TOK_IMPLICIT_HEIGHT_SYM:
        case TOK_COLOR_SYM:
        case TOK_VISIBLE_SYM:
        case TOK_SRC_SYM:
        case TOK_TEXT_SYM:
            identifier(p);
            break;
        case TOK_NUMBER:
            number(p);
            break;
        case TOK_LEFT_PARENTHESIS:
            expect(p, TOK_LEFT_PARENTHESIS);
            math_expression(p);
            expect(p, TOK_RIGHT_PARENTHESIS);
            break;
        default:
            unexpected_token(t);
    }
}

/// Term -> Factor | Factor MulOp Factor
static void
term(parser_handle p)
{
    size_t hook = ctx.ast.count;

    factor(p);

    if(is_mullop(peek(p).type))
    {
        struct ASTNode n = ast_make_node_op(eat(p).lexeme[0]);
        ast_insert_at(&ctx.ast, n, hook);

        term(p);
    }
}

/// MathExpression ->  Term | Term AddOp Term
static void
math_expression(parser_handle p)
{
    size_t hook = ctx.ast.count;

    term(p);

    if(is_addop(peek(p).type))
    {
        struct Token t = eat(p);

        switch(t.type)
        {
            case TOK_PLUS:
            {
                struct ASTNode n = ast_make_node_op('+');
                ast_insert_at(&ctx.ast, n, hook);
                break;
            }
            case TOK_MINUS:
            {
                struct ASTNode n = ast_make_node_op('+');
                ast_insert_at(&ctx.ast, n, hook);

                ctx.real_token = ctx.token;
                ctx.token = (struct Token){
                    .type = TOK_NUMBER,
                    .lexeme = "-1",
                    .lexeme_size = 2
                };

                ctx.fake_tokens[0] = (struct Token){
                    .type = TOK_TIMES,
                    .lexeme = "*",
                    .lexeme_size = 1
                };

                ctx.fake_tokens_count = 1;

                break;
            }
            default: unexpected_token(t);
        }

        math_expression(p);
    }
}

static void
boolean(parser_handle p)
{
    struct Token t = eat(p);

    switch(t.type)
    {
    case TOK_TRUE_SYM:
        ast_push(&ctx.ast, ast_make_node(TOK_TRUE_SYM));
        break;
    case TOK_FALSE_SYM:
        ast_push(&ctx.ast, ast_make_node(TOK_TRUE_SYM));
        break;
    default:
        unexpected_token(t);
    }
}

/// Type -> 'int' | 'real' | 'bool' | 'string'
static enum TokenType
type(parser_handle p)
{
    struct Token t = eat(p);

    switch(t.type)
    {
        case TOK_INT_SYM:
            ast_push(&ctx.ast, ast_make_node(TOK_INT_SYM));
            break;
        case TOK_REAL_SYM:
            ast_push(&ctx.ast, ast_make_node(TOK_REAL_SYM));
            break;
        case TOK_BOOL_SYM:
            ast_push(&ctx.ast, ast_make_node(TOK_BOOL_SYM));
            break;
        case TOK_STRING_SYM:
            ast_push(&ctx.ast, ast_make_node(TOK_STRING_SYM));
            break;
        default:
            unexpected_token(t);
    }

    return t.type;
}

/// StringExpression -> String | Identifier | String '+' StringExpression | Identifier '+' StringExpression
static void
string_expression(parser_handle p)
{
    struct Token t = peek(p);

    switch(t.type)
    {
        case TOK_STRING: string(p); break;
        case TOK_LOWERCASE_IDENTIFIER:
        case TOK_UPPERCASE_IDENTIFIER:
            identifier(p);
            break;
        default: unexpected_token(t);
    }

    if(peek(p).type == TOK_PLUS)
    {
        ast_push(&ctx.ast, ast_make_node_op('+'));
        string_expression(p);
    }
}

static void
property(parser_handle p)
{
    expect(p, TOK_PROPERTY_SYM);

    enum TokenType property_type = type(p);

    if(property_type == TOK_INT_SYM)
    {
        ast_push(&ctx.ast, ast_make_node(TOK_INT_SYM));
    }

    if(property_type == TOK_REAL_SYM)
    {
        ast_push(&ctx.ast, ast_make_node(TOK_REAL_SYM));
    }

    struct Token t = expect_t(p, TOK_LOWERCASE_IDENTIFIER);
    expect(p, TOK_COLON);

    ast_push(&ctx.ast, ast_make_node(TOK_ASSIGNMENT));
    ast_push(&ctx.ast, ast_make_node_lowercase_identifier(t.lexeme, t.lexeme_size));

    size_t hook = ctx.ast.count;

    if(property_type == TOK_INT_SYM || property_type == TOK_REAL_SYM)
    {
        math_expression(p);
        ast_reverse_from(&ctx.ast, hook);
        ast_fix_identifiers(&ctx.ast, hook);
    }

    if(property_type == TOK_BOOL_SYM)
    {
        boolean(p);
    }

    if(property_type == TOK_STRING_SYM)
    {
        string_expression(p);
    }
}

static void
id(parser_handle p)
{
    expect(p, TOK_ID_SYM);

    expect(p, TOK_COLON);

    struct Token t = expect_t(p, TOK_LOWERCASE_IDENTIFIER);
    ast_push(&ctx.ast, ast_make_node_lexeme(t.lexeme, t.lexeme_size, TOK_ID_SYM));
}

static void
internal_attribute_int(parser_handle p, enum TokenType t)
{
    expect(p, t);

    expect(p, TOK_COLON);

    ast_push(&ctx.ast, ast_make_node(t));

    ast_push(&ctx.ast, ast_make_node(TOK_BEGIN_EXPR));

    size_t hook = ctx.ast.count;

    math_expression(p);
    ast_reverse_from(&ctx.ast, hook);
    ast_fix_identifiers(&ctx.ast, hook);

    ast_push(&ctx.ast, ast_make_node(TOK_END_EXPR));
}

static void
width(parser_handle p)
{
    internal_attribute_int(p, TOK_WIDTH_SYM);
}

static void
height(parser_handle p)
{
    internal_attribute_int(p, TOK_HEIGHT_SYM);
}

static void
implicit_width(parser_handle p)
{
    internal_attribute_int(p, TOK_IMPLICIT_WIDTH_SYM);
}

static void
implicit_height(parser_handle p)
{
    internal_attribute_int(p, TOK_IMPLICIT_HEIGHT_SYM);
}

static void
x(parser_handle p)
{
    internal_attribute_int(p, TOK_X_SYM);
}

static void
y(parser_handle p)
{
    internal_attribute_int(p, TOK_Y_SYM);
}

static void
color(parser_handle p)
{
    expect(p, TOK_COLOR_SYM);

    expect(p, TOK_COLON);

    struct Token t = expect_t(p, TOK_STRING);

    ast_push(&ctx.ast, ast_make_node(TOK_COLOR_SYM));
    ast_push(&ctx.ast, ast_make_node_string(t.lexeme, t.lexeme_size));
}

static void
visible(parser_handle p)
{
    expect(p, TOK_VISIBLE_SYM);

    expect(p, TOK_COLON);

    struct Token t = eat(p);

    ast_push(&ctx.ast, ast_make_node(TOK_VISIBLE_SYM));

    switch(t.type)
    {
        case TOK_TRUE_SYM: ast_push(&ctx.ast, ast_make_node(TOK_TRUE_SYM)); return;
        case TOK_FALSE_SYM: ast_push(&ctx.ast, ast_make_node(TOK_FALSE_SYM)); return;
        default: unexpected_token(t);
    }
}

static void
src(parser_handle p)
{
    expect(p, TOK_SRC_SYM);

    expect(p, TOK_COLON);

    struct Token t = expect_t(p, TOK_STRING);

    ast_push(&ctx.ast, ast_make_node(TOK_SRC_SYM));
    ast_push(&ctx.ast, ast_make_node_string(t.lexeme, t.lexeme_size));
}

static void
text(parser_handle p)
{
    expect(p, TOK_TEXT_SYM);

    expect(p, TOK_COLON);

    ast_push(&ctx.ast, ast_make_node(TOK_TEXT_SYM));

    struct Token t = expect_t(p, TOK_STRING);

    ast_push(&ctx.ast, ast_make_node_string(t.lexeme, t.lexeme_size));
}

static void
assignment(parser_handle p)
{
    struct Token t = expect_t(p, TOK_LOWERCASE_IDENTIFIER);
    expect(p, TOK_COLON);

    ast_push(&ctx.ast, ast_make_node(TOK_ASSIGNMENT));
    ast_push(&ctx.ast, ast_make_node_lowercase_identifier(t.lexeme, t.lexeme_size));

    if(peek(p).type == TOK_TRUE_SYM || peek(p).type == TOK_FALSE_SYM)
    {
        boolean(p);
    }
    else
    {
        ast_push(&ctx.ast, ast_make_node(TOK_BEGIN_EXPR));

        size_t hook = ctx.ast.count;

        math_expression(p);
        ast_reverse_from(&ctx.ast, hook);
        ast_fix_identifiers(&ctx.ast, hook);

        ast_push(&ctx.ast, ast_make_node(TOK_BEGIN_EXPR));
    }
}

static void
element(parser_handle p);

/// BodyMember -> Assignment | Element
/// Assignment -> LowercaseIdentifier ':' MathExpression | 'property' Type LowercaseIdentifier ':' MathExpression
static void
body_member(parser_handle p)
{
    struct Token t = peek(p);

    switch(t.type)
    {
    case TOK_PROPERTY_SYM: property(p); return;
    case TOK_ID_SYM: id(p); return;
    case TOK_WIDTH_SYM: width(p); return;
    case TOK_HEIGHT_SYM: height(p); return;
    case TOK_IMPLICIT_WIDTH_SYM: implicit_width(p); return;
    case TOK_IMPLICIT_HEIGHT_SYM: implicit_height(p); return;
    case TOK_X_SYM: x(p); return;
    case TOK_Y_SYM: y(p); return;
    case TOK_COLOR_SYM: color(p); return;
    case TOK_VISIBLE_SYM: visible(p); return;
    case TOK_SRC_SYM: src(p); return;
    case TOK_TEXT_SYM: text(p); return;
    case TOK_LOWERCASE_IDENTIFIER: assignment(p); return;
    case TOK_UPPERCASE_IDENTIFIER:
        ast_push(&ctx.ast, ast_make_node(TOK_ELEMENT));
        element(p);
        break;
    default:
        unexpected_token(t);
    }
}

/// Body -> BodyMember*
static void
body(parser_handle p)
{
    while(ctx.token.type != TOK_RIGHT_BRACE)
    {
        body_member(p);
    }
}

/// Element -> Uppercase_Identifier '{' Body '}'
static void
element(parser_handle p)
{
    struct Token t = expect_t(p, TOK_UPPERCASE_IDENTIFIER);

    ast_push(&ctx.ast, ast_make_node_uppercase_identifier(t.lexeme, t.lexeme_size));

    expect(p, TOK_LEFT_BRACE);

    body(p);

    expect(p, TOK_RIGHT_BRACE);
}

/// Grammar:
/// Element -> Uppercase_Identifier '{' Body '}'
/// Body -> BodyMember*
/// BodyMember -> Assignment | Element
/// Assignment -> LowercaseIdentifier ':' MathExpression | 'property' Type LowercaseIdentifier ':' MathExpression
/// Type -> 'int' | 'real' | 'bool' | 'string'
/// MathExpression ->  true | false | Term | Term AddOp Term
/// Term -> Factor | Factor MulOp Factor
/// Factor -> Number | Identifier | '(' MathExpression ')'
/// AddOp -> + | -
/// MulOp -> * | /
/// StringExpression -> String | Identifier | String '+' StringExpression | Identifier '+' StringExpression
/// Identifier -> LowercaseIdentifier | Uppercase_Identifier | LowercaseIdentifier.Identifier | Uppercase_Identifier.Identifier
/// LowercaseIdentifier -> [A-z][A-z0-9]*
/// Uppercase_Identifier -> [A-Z][A-z0-9]*
/// String -> '"' .* '"'
struct AST
lol_parse(parser_handle p)
{
    if(setjmp(jump_buffer) != 0)
    {
        struct AST ast = {
            .error = 1
        };

        return ast;
    }

    _nextsym(p);
    ast_push(&ctx.ast, ast_make_node(TOK_ELEMENT));
    element(p);

    expect_t(p, TOK_NONE);
    return ctx.ast;
}
