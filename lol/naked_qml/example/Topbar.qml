Item
{
    id: root
    property string default_color: green.color
    property int btn_width: 0

    signal colorChosen(string color)
    signal textChosen()

    Rectangle
    {
        anchors.fill: parent
        color: "#333"
        property alias btn_width: root.btn_width

        Button {
            id: white
            x: parent.x
            width: parent.btn_width
            height: parent.height
            color: "#eee"
        }

        Button {
            id: black
            anchors.left: white.right
            width: parent.btn_width
            height: parent.height
            color: "#111"
        }

        Button {
            id: green
            anchors.left: black.right
            width: parent.btn_width
            height: parent.height
            color: "#3f9384"
        }

        Button {
            id: red
            anchors.left: green.right
            width: parent.btn_width
            height: parent.height
            color: "#e36900"
        }

        Button {
            id: yellow
            anchors.left: red.right
            width: parent.btn_width
            height: parent.height
            color: "#fad768"
        }

        Button {
            id: text
            anchors.left: yellow.right
            width: parent.btn_width
            height: parent.height
            color: "#fff"
            text: "T"
        }
    }
}
