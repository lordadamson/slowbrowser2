Item
{
    id: root
    x: 120
    y: 100
    height: 20
    width: metrics.advanceWidth
    readonly property TextEdit text_edit: edit

    property string text: ""
    property string color: "#111"

    TextMetrics
    {
        id: metrics
        text: edit.text
        font.pointSize: edit.font.pointSize
    }

    TextEdit
    {
        id: edit
        anchors.fill: parent
        text: root.text
        font.pointSize: root.height
        color: root.color

        MouseArea
        {
            anchors.fill: parent
            propagateComposedEvents: true
        }
    }

    Rectangle
    {
        id: border
        x: -4
        y: -4
        height: root.height + 8
        width: metrics.boundingRect.width + 10
        color: "#00000000" // transparent
        border.color: "black"
        border.width: 3
        visible: edit.activeFocus
    }

    Rectangle
    {
        id: handle
        x: (metrics.boundingRect.width / 2) - (width / 2)
        y: -14
        height: 12
        width: 12
        color: "black"
        border.width: 3
        visible: edit.activeFocus

        MouseArea
        {
            anchors.fill: parent
            property bool ispressed: false
        }
    }
}
