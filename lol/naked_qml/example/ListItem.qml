Rectangle {
    height: 20
    width: parent.width

    color: "#ddd"
    Text {
        anchors.centerIn: parent
        text: name
    }
}