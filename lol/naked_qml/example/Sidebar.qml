Item
{
    Rectangle
    {
        id: sidebar
        color: "#333"
        anchors.fill: parent

        PaperList {
            width: parent.width
            height: parent.height
        }
    }
}
