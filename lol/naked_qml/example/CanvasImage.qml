Item {
    id: root
    readonly property Image image: img

    Image
    {
        id: img
    }

    MouseArea
    {
        anchors.fill: img
        property bool ispressed: false

        property real dragx: 0.0
        property real dragy: 0.0

        onPressed: {
            ispressed = true
            dragx = mouseX
            dragy = mouseY
        }

        onReleased: ispressed = false
    }
}
