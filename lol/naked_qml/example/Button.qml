Item {
    id: root
    property alias color: btn.color
    property string text: ""

    Rectangle {
        anchors.fill: parent
        id: btn

        MouseArea {
            anchors.fill: parent
        }

        Text {
            text: root.text
            anchors.centerIn: parent
        }
    }
}
