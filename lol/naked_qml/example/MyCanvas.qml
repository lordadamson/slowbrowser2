Rectangle {
    color: "#eee"
    property string selected_tool: "brush"
    property string selected_color: "#111"

    Backend {
        id: backend
    }

    Rectangle {
        id: breh
        x: 50
        y: 50
        z: 1
        height: 50
        width: 50
        color: "black"

        MouseArea {
            property bool dragging: false
            anchors.fill: parent
            drag.target: breh
            drag.minimumX : 0
            drag.minimumY : 0
            drag.maximumX : canvas.width
            drag.maximumY : canvas.height
        }
    }

    Canvas {
        id: canvas
        anchors.fill: parent

        property real lastX
        property real lastY

        DropArea {
            anchors.fill: parent
        }

        MouseArea {
            id: paint_area
            anchors.fill: parent
        }
    }
}
