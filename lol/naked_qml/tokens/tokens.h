#pragma once

#include <stdbool.h>
#include <stddef.h>

enum TokenType {
    TOK_NONE,
    TOK_IMPORT_SYM,
    TOK_REQUIRED_SYM,
    TOK_READONLY_SYM,
    TOK_SIGNAL_SYM,
    TOK_PROPERTY_SYM,
    TOK_AS_SYM,
    TOK_ID_SYM,
    TOK_WIDTH_SYM,
    TOK_HEIGHT_SYM,
    TOK_IMPLICIT_WIDTH_SYM,
    TOK_IMPLICIT_HEIGHT_SYM,
    TOK_X_SYM,
    TOK_Y_SYM,
    TOK_COLOR_SYM,
    TOK_VISIBLE_SYM,
    TOK_SRC_SYM,
    TOK_TEXT_SYM,
    TOK_UPPERCASE_IDENTIFIER,
    TOK_LOWERCASE_IDENTIFIER,
    TOK_STRING,
    TOK_STRING_SYM,
    TOK_BOOL_SYM,
    TOK_TRUE_SYM,
    TOK_FALSE_SYM,
    TOK_DOUBLE_SYM,
    TOK_REAL_SYM,
    TOK_INT_SYM,
    TOK_LIST_SYM,
    TOK_VAR_SYM,
    TOK_ALIAS_SYM,
    TOK_NUMBER,
    TOK_INT,
    TOK_DOUBLE,
    TOK_LEFT_BRACE,
    TOK_RIGHT_BRACE,
    TOK_LEFT_PARENTHESIS,
    TOK_RIGHT_PARENTHESIS,
    TOK_LEFT_SQUARE_BRACKET,
    TOK_RIGHT_SQUARE_BRACKET,
    TOK_COLON,
    TOK_SEMICOLON,
    TOK_DOT,
    TOK_PLUS,
    TOK_MINUS,
    TOK_TIMES,
    TOK_DIVIDE,
    TOK_ELEMENT,
    TOK_ASSIGNMENT,
    TOK_BEGIN_EXPR,
    TOK_END_EXPR,
    TOK_UNKNOWN
};

struct Token
{
    enum TokenType type;

    /// a pointer to the beginning of a lexeme in the file_content
    /// array that is passed to the \ref lexer_init() function
    /// we do not manage this pointer, the caller must not free
    /// file_content until the lexing is complete
    const char* lexeme;
    size_t lexeme_size;
    int eof;
    size_t line;
    size_t column;
};

extern const char* const symbols[];
extern const size_t symbols_count;

enum TokenType
string_to_tokentype(const char*);

const char*
tokentype_to_string(enum TokenType t);

bool
is_attribute(enum TokenType t);
