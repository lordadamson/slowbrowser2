#include "tokens.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const char* const symbols[] = {
        "import",
        "required",
        "property",
        "readonly",
        "signal",
        "int",
        "real",
        "double",
        "string",
        "bool",
        "true",
        "false",
        "list",
        "var",
        "as",
        "alias",
        "id",
        "width",
        "implicit_width",
        "implicit_height",
        "height",
        "x",
        "y",
        "color",
        "visible",
        "src",
        "text",
        "{",
        "}",
        "[",
        "]",
        "(",
        ")",
        ":",
        ";",
        ".",
        "+",
        "-",
        "*",
        "/",
        };

size_t const symbols_count = sizeof(symbols)/sizeof(symbols[0]);

bool
is_attribute(enum TokenType t)
{
    return
        t == TOK_LOWERCASE_IDENTIFIER ||
        t == TOK_UPPERCASE_IDENTIFIER ||
        t == TOK_X_SYM                ||
        t == TOK_Y_SYM                ||
        t == TOK_WIDTH_SYM            ||
        t == TOK_HEIGHT_SYM           ||
        t == TOK_IMPLICIT_WIDTH_SYM   ||
        t == TOK_IMPLICIT_HEIGHT_SYM  ||
        t == TOK_COLOR_SYM            ||
        t == TOK_VISIBLE_SYM          ||
        t == TOK_TEXT_SYM             ||
        t == TOK_SRC_SYM;
}

enum TokenType
string_to_tokentype(const char* string)
{
    if(strcmp(string, "import") == 0)
    {
        return TOK_IMPORT_SYM;
    }
    if(strcmp(string, "required") == 0)
    {
        return TOK_REQUIRED_SYM;
    }
    if(strcmp(string, "property") == 0)
    {
        return TOK_PROPERTY_SYM;
    }
    if(strcmp(string, "readonly") == 0)
    {
        return TOK_READONLY_SYM;
    }
    if(strcmp(string, "signal") == 0)
    {
        return TOK_SIGNAL_SYM;
    }
    if(strcmp(string, "int") == 0)
    {
        return TOK_INT_SYM;
    }
    if(strcmp(string, "real") == 0)
    {
        return TOK_REAL_SYM;
    }
    if(strcmp(string, "double") == 0)
    {
        return TOK_DOUBLE_SYM;
    }
    if(strcmp(string, "string") == 0)
    {
        return TOK_STRING_SYM;
    }
    if(strcmp(string, "bool") == 0)
    {
        return TOK_BOOL_SYM;
    }
    if(strcmp(string, "true") == 0)
    {
        return TOK_TRUE_SYM;
    }
    if(strcmp(string, "false") == 0)
    {
        return TOK_FALSE_SYM;
    }
    if(strcmp(string, "list") == 0)
    {
        return TOK_LIST_SYM;
    }
    if(strcmp(string, "var") == 0)
    {
        return TOK_VAR_SYM;
    }
    if(strcmp(string, "as") == 0)
    {
        return TOK_AS_SYM;
    }
    if(strcmp(string, "alias") == 0)
    {
        return TOK_ALIAS_SYM;
    }
    if(strcmp(string, "id") == 0)
    {
        return TOK_ID_SYM;
    }
    if(strcmp(string, "width") == 0)
    {
        return TOK_WIDTH_SYM;
    }
    if(strcmp(string, "height") == 0)
    {
        return TOK_HEIGHT_SYM;
    }
    if(strcmp(string, "implicit_width") == 0)
    {
        return TOK_IMPLICIT_WIDTH_SYM;
    }
    if(strcmp(string, "implicit_height") == 0)
    {
        return TOK_IMPLICIT_HEIGHT_SYM;
    }
    if(strcmp(string, "x") == 0)
    {
        return TOK_X_SYM;
    }
    if(strcmp(string, "y") == 0)
    {
        return TOK_Y_SYM;
    }
    if(strcmp(string, "color") == 0)
    {
        return TOK_COLOR_SYM;
    }
    if(strcmp(string, "visible") == 0)
    {
        return TOK_VISIBLE_SYM;
    }
    if(strcmp(string, "src") == 0)
    {
        return TOK_SRC_SYM;
    }
    if(strcmp(string, "text") == 0)
    {
        return TOK_TEXT_SYM;
    }
    if(strcmp(string, "{") == 0)
    {
        return TOK_LEFT_BRACE;
    }
    if(strcmp(string, "}") == 0)
    {
        return TOK_RIGHT_BRACE;
    }
    if(strcmp(string, "[") == 0)
    {
        return TOK_LEFT_SQUARE_BRACKET;
    }
    if(strcmp(string, "]") == 0)
    {
        return TOK_RIGHT_SQUARE_BRACKET;
    }
    if(strcmp(string, "(") == 0)
    {
        return TOK_LEFT_PARENTHESIS;
    }
    if(strcmp(string, ")") == 0)
    {
        return TOK_RIGHT_PARENTHESIS;
    }
    if(strcmp(string, ":") == 0)
    {
        return TOK_COLON;
    }
    if(strcmp(string, ";") == 0)
    {
        return TOK_SEMICOLON;
    }
    if(strcmp(string, ".") == 0)
    {
        return TOK_DOT;
    }
    if(strcmp(string, "+") == 0)
    {
        return TOK_PLUS;
    }
    if(strcmp(string, "-") == 0)
    {
        return TOK_MINUS;
    }
    if(strcmp(string, "*") == 0)
    {
        return TOK_TIMES;
    }
    if(strcmp(string, "/") == 0)
    {
        return TOK_DIVIDE;
    }

    fprintf(stderr, "unknown symbol: %s\n", string);
    abort();
}

const char*
tokentype_to_string(enum TokenType t)
{
    switch(t)
    {
    case TOK_IMPORT_SYM: return "import";
    case TOK_REQUIRED_SYM: return "required";
    case TOK_PROPERTY_SYM: return "property";
    case TOK_READONLY_SYM: return "readonly";
    case TOK_SIGNAL_SYM: return "signal";
    case TOK_INT_SYM: return "int";
    case TOK_REAL_SYM: return "real";
    case TOK_DOUBLE_SYM: return "double";
    case TOK_STRING_SYM: return "string";
    case TOK_BOOL_SYM: return "bool";
    case TOK_TRUE_SYM: return "true";
    case TOK_FALSE_SYM: return "false";
    case TOK_LIST_SYM: return "list";
    case TOK_VAR_SYM: return "var";
    case TOK_AS_SYM: return "as";
    case TOK_ALIAS_SYM: return "alias";
    case TOK_ID_SYM: return "id";
    case TOK_WIDTH_SYM: return "width";
    case TOK_HEIGHT_SYM: return "height";
    case TOK_IMPLICIT_WIDTH_SYM: return "implicit_width";
    case TOK_IMPLICIT_HEIGHT_SYM: return "implicit_height";
    case TOK_X_SYM: return "x";
    case TOK_Y_SYM: return "y";
    case TOK_COLOR_SYM: return "color";
    case TOK_VISIBLE_SYM: return "visible";
    case TOK_SRC_SYM: return "src";
    case TOK_TEXT_SYM: return "text";
    case TOK_LEFT_BRACE: return "{";
    case TOK_RIGHT_BRACE: return "}";
    case TOK_LEFT_SQUARE_BRACKET: return "[";
    case TOK_RIGHT_SQUARE_BRACKET: return "]";
    case TOK_LEFT_PARENTHESIS: return "(";
    case TOK_RIGHT_PARENTHESIS: return ")";
    case TOK_COLON: return ":";
    case TOK_SEMICOLON: return ";";
    case TOK_DOT: return ".";
    case TOK_PLUS: return "+";
    case TOK_MINUS: return "-";
    case TOK_TIMES: return "*";
    case TOK_DIVIDE: return "/";
    case TOK_NONE: return "TOK_NONE";
    case TOK_UPPERCASE_IDENTIFIER: return "TOK_UPPERCASE_IDENTIFIER";
    case TOK_LOWERCASE_IDENTIFIER: return "TOK_LOWERCASE_IDENTIFIER";
    case TOK_STRING: return "TOK_STRING";
    case TOK_NUMBER: return "TOK_NUMBER";
    case TOK_INT: return "TOK_INT";
    case TOK_DOUBLE: return "TOK_DOUBLE";
    case TOK_ELEMENT: return "TOK_ELEMENT";
    case TOK_ASSIGNMENT: return "TOK_ASSIGNMENT";
    case TOK_BEGIN_EXPR: return "TOK_BEGIN_EXPR";
    case TOK_END_EXPR: return "TOK_END_EXPR";
    case TOK_UNKNOWN: return "TOK_UNKNOWN";
    default: abort();
    }
}
