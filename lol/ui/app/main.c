#include "raylib.h"

#include <stdlib.h>
#include <dlfcn.h>
#include <stdio.h>

struct error_t
{
    char what[1024];
    int code;
};



struct cat_t;

struct cat_interface_t
{
    struct cat_t* (*cat_init)(void);
    void (*cat_update)(struct cat_t*);
};

struct cat_interface_result_t
{
    struct error_t error;
    struct cat_interface_t interface;  
};

struct cat_interface_result_t
cat_reload()
{
    system("cd " build_dir "; cmake -S .. -B .");
    fprintf(stderr, "cd " build_dir "; cmake -S .. -B .\n");
    fprintf(stderr, "libcat: %s\n", libcat);

    static void* handle;

    if(handle != NULL)
    {
        dlclose(handle);
    }

    handle = dlopen(libcat, RTLD_NOW);
    char *error = NULL;

    struct cat_interface_result_t result = {0};
    struct cat_interface_t ci = {0};

    if(!handle)
    {
        goto error;
    }

    dlerror();    /* Clear any existing error */

    *(void **) (&ci.cat_init)   = dlsym(handle, "cat_init");

    if ((error = dlerror()) != NULL)
    {
        goto error;
    }

    *(void **) (&ci.cat_update) = dlsym(handle, "cat_update");

    if ((error = dlerror()) != NULL)
    {
        goto error;
    }

    result.interface = ci;
    return result;

    error:
    result.error.code = -1;
    snprintf(result.error.what, 1024, "%s", dlerror());
    return result;
}

int main(void)
{
    SetConfigFlags(FLAG_WINDOW_RESIZABLE);
    InitWindow(800, 450, "My Silly Language");
    BeginDrawing();
    EndDrawing();
    MaximizeWindow();
    EnableEventWaiting();

    struct cat_t* cat_ctx = NULL;
    struct cat_interface_t i = {0};

    while (!WindowShouldClose())
    {
        // if R is pressed
        // recompile cat and reload it

        // init cat if not initialized
        if(cat_ctx == NULL || IsKeyPressed(KEY_R))
        {
            struct cat_interface_result_t r = cat_reload();

            if(r.error.code != 0)
            {
                fprintf(stderr, "error: %s\n", r.error.what);
                continue;
            }

            i = r.interface;

            cat_ctx = i.cat_init();
        }

        // call cat logic
        i.cat_update(cat_ctx);
    }

    CloseWindow();

    return 0;
}
