set(sources
    main.c
)

if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    set(lflags
        "-framework Foundation"
        "-framework Cocoa"
        "-framework Metal"
        "-framework CoreAudio"
        "-framework CoreVideo"
        "-framework CoreServices"
        "-framework GameController"
        "-framework ForceFeedback"
        "-framework IOKit"
        "-framework AudioToolbox"
        "-framework AVFoundation"
        "-framework Carbon"
    )
endif()

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set(lflags
        "m"
        "pthread"
        "dl"
    )
endif()

set(exename cat_app)

add_executable(${exename}
        ${sources}
)

if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    target_include_directories(${exename}
        PRIVATE
                    ${CMAKE_SOURCE_DIR}/3rdparties/macos/raylib/include/
    )

    target_link_libraries(${exename}
       PRIVATE
           ${CMAKE_SOURCE_DIR}/3rdparties/macos/raylib/lib/libraylib.500.dylib
           ${lflags}
    )
endif()

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    target_include_directories(${exename}
        PRIVATE
                    ${CMAKE_SOURCE_DIR}/3rdparties/linux/raylib/include/
    )

    target_link_libraries(${exename}
       PRIVATE
           ${CMAKE_SOURCE_DIR}/3rdparties/linux/raylib/lib/libraylib.a
           ${lflags}
    )
endif()


target_compile_definitions(${exename}
    PRIVATE
        "-Dbuild_dir=\"${CMAKE_SOURCE_DIR}/build/\""
)

if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    target_compile_definitions(${exename}
        PRIVATE
            "-Dlibcat=\"${CMAKE_SOURCE_DIR}/build/ui/lib/libcat.so\""
    )
endif()

if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    target_compile_definitions(${exename}
        PRIVATE
            "-Dlibcat=\"${CMAKE_SOURCE_DIR}/build/ui/lib/libcat.dylib\""
    )
endif()
