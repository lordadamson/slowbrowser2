# UI

I learned at some point in my life that the basic blocks of UI graphics are

- Rectangles, optionally rounded, which can become circles
- Text
- Images

Using these building blocks you're able draw more complex things

- buttons
- Textboxes
- Containers
- Sliders
- etc.

Using only those I once built a complete UI for a file manager.

Let's see if my theory holds over time.