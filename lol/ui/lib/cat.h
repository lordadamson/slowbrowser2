#pragma once

#include "../../engine/engine.h"

struct cat_t;

struct cat_t*
cat_init(struct engine_t*);

struct cat_t*
cat_init2(struct object_t obj);

void
cat_free(struct cat_t*);

void
cat_update(struct cat_t*);

bool
cat_should_close(struct cat_t*);

int
cat_key_press(struct cat_t* c);

bool
cat_mouse_pressed(struct cat_t* c, int button);

bool
cat_mouse_down(struct cat_t* c, int button);

bool
cat_mouse_released(struct cat_t* c, int button);

int
cat_mouse_x(struct cat_t* c);

int
cat_mouse_y(struct cat_t* c);

bool
cat_point_rect_hit_test(int px, int py, int x, int y, int w, int h);

struct cat_texture_t
{
    struct Texture* texture;
    size_t width;
    size_t height;
};

struct cat_texture_t
cat_texture_from_file(const char* const file_path);

void
cat_texture_release(struct Texture*);

int64_t
cat_window_width(struct cat_t*);

int64_t
cat_window_height(struct cat_t*);
