#include "cat.h"

#include <raylib.h>

#include <stdlib.h>
#include <string.h>

struct cat_t
{
    struct engine_t* engine;
    struct object_t root_object;
};

struct cat_t*
cat_init(struct engine_t* e)
{
    struct cat_t* ctx = malloc(sizeof(struct cat_t));
    ctx->engine = e;
    SetConfigFlags(FLAG_WINDOW_RESIZABLE);
    InitWindow(800, 450, "Kitty");
    BeginDrawing();
    EndDrawing();
    MaximizeWindow();
    EnableEventWaiting();

    return ctx;
}

struct cat_t*
cat_init2(struct object_t obj)
{
    struct cat_t* ctx = malloc(sizeof(struct cat_t));
    ctx->root_object = obj;
    SetConfigFlags(FLAG_WINDOW_RESIZABLE);
    InitWindow(800, 450, "Kitty");
    BeginDrawing();
    EndDrawing();
    MaximizeWindow();
    EnableEventWaiting();

    return ctx;
}

void
cat_free(struct cat_t* e)
{
    free(e);
}

bool
cat_should_close(struct cat_t* e)
{
    return WindowShouldClose();
}

int
cat_key_press(struct cat_t* c)
{
    return GetKeyPressed();
}

bool
cat_mouse_pressed(struct cat_t* c, int button)
{
    return IsMouseButtonPressed(button);
}

bool
cat_mouse_down(struct cat_t* c, int button)
{
    return IsMouseButtonDown(button);
}

bool
cat_mouse_released(struct cat_t* c, int button)
{
    return IsMouseButtonReleased(button);
}


int
cat_mouse_x(struct cat_t* c)
{
    return GetMouseX();
}

int
cat_mouse_y(struct cat_t* c)
{
    return GetMouseY();
}

bool
cat_point_rect_hit_test(int px, int py, int x, int y, int w, int h)
{
    if(px > x + w) return false;
    if(py > y + h) return false;
    if(px < x) return false;
    if(py < y) return false;
    return true;
}

struct cat_texture_t
cat_texture_from_file(const char* const file_path)
{
    struct cat_texture_t ct = {0};

    Image img = LoadImage(file_path);

    if(img.data == NULL)
    {
        return ct;
    }

    ct.texture = malloc(sizeof(Texture));
    memset(ct.texture, 0, sizeof(Texture));

    *ct.texture = LoadTextureFromImage(img);

    ct.width = img.width;
    ct.height = img.height;

    UnloadImage(img);

    return ct;
}

void
cat_texture_release(Texture* texture)
{
    if(texture == NULL)
    {
        return;
    }

    UnloadTexture(*texture);
    free(texture);
}

int64_t
cat_window_width(struct cat_t* c)
{
    return GetScreenWidth();
}

int64_t
cat_window_height(struct cat_t* c)
{
    return GetScreenHeight();
}

void
cat_update(struct cat_t* c)
{
    BeginDrawing();

        ClearBackground(RAYWHITE);


        for(size_t i = 0; i < engine_object_count(*c->engine); i++)
        {
            struct object_t object = c->engine->objects[i];

            switch(object.type)
            {
            case Object_Rectangle:
            {
                Color color = CLITERAL(Color){ object.color.r, object.color.g, object.color.b, object.color.a };

                int64_t x = engine_object_x(*c->engine, i);
                int64_t y = engine_object_y(*c->engine, i);
                int64_t w = engine_object_width(*c->engine, i);
                int64_t h = engine_object_height(*c->engine, i);

                DrawRectangle(x, y, w, h, color);
                break;
            }
            case Object_Text:
            {
                struct string_t string = engine_object_text(*c->engine, i);
                Color color = CLITERAL(Color){ object.color.r, object.color.g, object.color.b, object.color.a };
                int64_t x = engine_object_x(*c->engine, i);
                int64_t y = engine_object_y(*c->engine, i);
                int64_t h = engine_object_height(*c->engine, i);

                DrawText(string.string, x, y, h, color);
                break;
            }
            case Object_Image:
            {
                int64_t x = engine_object_x(*c->engine, i);
                int64_t y = engine_object_y(*c->engine, i);
                int64_t w = engine_object_width(*c->engine, i);
                int64_t h = engine_object_height(*c->engine, i);
                int64_t iw = engine_object_implicit_width(*c->engine, i);
                int64_t ih = engine_object_implicit_height(*c->engine, i);
                Texture* texture = engine_object_texture(*c->engine, i);

                if(w == iw && h == ih)
                {
                    DrawTexture(*texture, x, y, WHITE);
                }
                else
                {
                    float scale = (float)w / ((float)iw + 0.0000000001f);
                    DrawTextureEx(*texture, (Vector2){x, y}, 0, scale, WHITE);
                }

                break;
            }
            default: break;
            }
        }

    EndDrawing();
}
