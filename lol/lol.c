#include "lol.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "naked_qml/tokens/tokens.h"
#include "naked_qml/lexer/lexer.h"
#include "naked_qml/parser/parser.h"
#include "naked_qml/evaluator/eval.h"

#include "common/stb_ds.h"
#include "common/mem.h"
#include "ui/lib/cat.h"

struct lol_t
{
    struct cat_t* cat;
    struct engine_t engine;
    struct object_t root_object;
    memory_handle memory;
    struct lol_config_t config;
};

void error(const char msg[])
{
    fprintf(stderr, "%s\n", msg);
}

struct Token
nextsym(void * user_data)
{
    lexer_handle lx = *(lexer_handle*)user_data;

    struct Token t = lex(lx);
    char buf[1024] = {0};
    snprintf(buf, t.lexeme_size+1, "%s", t.lexeme);
    printf("%s\n", buf);
    fflush(stdout);
    return t;
}

static const char*
file_read(const char* const path)
{
    struct stat buf;
    int exists = (stat(path, &buf) == 0);

    if(!(S_ISREG(buf.st_mode) && exists))
    {
        return NULL;
    }

    if(buf.st_size < 0)
    {
        return NULL;
    }

    if(buf.st_size == 0)
    {
        return NULL;
    }

    int fd = open(path, O_RDONLY);

    if(fd < 0)
    {
        return NULL;
    }

    char* output = malloc(buf.st_size + 1);

    ssize_t bytes_read = read(fd, output, buf.st_size);

    output[buf.st_size] = 0;

    close(fd);

    if(bytes_read != buf.st_size)
    {
        free(output);
        return NULL;
    }

    return output;
}

struct lex_parse_result_t
{
    struct AST ast;
    int error;
};

static struct lex_parse_result_t
lex_and_parse(const char* const qml)
{
    struct lex_parse_result_t result = {0};

    memory_handle memory;

    size_t lexer_needs = lexer_memory_needed();
    size_t parser_needs = parser_memory_needed(1);

    size_t memory_needed = lexer_needs + parser_needs;

    memory = lol_memory_init(memory_needed);

    struct memory_block_t lexer_memory = lol_alloc(memory, lexer_needs);
    struct memory_block_t parser_memory = lol_alloc(memory, parser_needs);

    lexer_handle lx = lexer_init(qml, lexer_memory);

    if(lx == -1)
    {
        fprintf(stderr, "lexer initialization failed\n");
        abort();
    }

    parser_handle p = parser_init(&lx, parser_memory, 1);

    result.ast = lol_parse(p);

    if(result.ast.error != 0)
    {
        fprintf(stderr, "parsing failed\n");
        lol_memory_deinit(memory);
        result.error = 1;
        return result;
    }

    for(size_t i = 0; i < result.ast.count; i++)
    {
        printf("%s ", tokentype_to_string(result.ast.nodes[i].type));

        switch(result.ast.nodes[i].type)
        {
        case TOK_LOWERCASE_IDENTIFIER:
        case TOK_UPPERCASE_IDENTIFIER:
        case TOK_STRING:
        case TOK_INT_SYM:
        case TOK_REAL_SYM:
        case TOK_BOOL_SYM:
        case TOK_STRING_SYM:
            printf("%s", result.ast.nodes[i].value.string);
            break;
        case TOK_DOUBLE:
            printf("%f", result.ast.nodes[i].value.real);
            break;
        case TOK_INT:
            printf("%" PRIi64, result.ast.nodes[i].value.integer);
            break;
        default:
            break;
        }

        printf("\n");
    }

    fflush(stdout);

    return result;
}

static struct lex_parse_result_t
lex_and_parse_from_file(const char* const file_path)
{
    struct lex_parse_result_t result = {0};

    const char* qml = file_read(file_path);

    if(qml == NULL)
    {
        fprintf(stderr, "couldn't read %s\n", file_path);
        result.error = 1;
        return result;
    }

    return lex_and_parse(qml);
}

struct tree_node_t
{
    struct AST ast;
    struct tree_node_t* children;
};

static void
unique_symbols(struct symbols_t* symbols)
{
    if(symbols == NULL || symbols->symbols_count == 0)
    {
        return;
    }

    size_t unique_count = 0;

    for (size_t i = 0; i < symbols->symbols_count; ++i)
    {
        bool is_duplicate = false;

        for (size_t j = 0; j < unique_count; ++j)
        {
            if (strcmp(symbols->symbols[i].string, symbols->symbols[j].string) == 0)
            {
                is_duplicate = true;
                break;
            }
        }

        if (!is_duplicate)
        {
            symbols->symbols[unique_count] = symbols->symbols[i];
            ++unique_count;
        }
    }

    symbols->symbols_count = unique_count;
}

struct dependency_tree_result_t
{
    struct tree_node_t root;
    int error;
};

static struct tree_node_t
tree_push(struct tree_node_t node, struct AST to_push)
{
    struct tree_node_t new = {0};
    new.ast = to_push;
    arrpush(node.children, new);
    return new;
}

static struct dependency_tree_result_t
build_dependency_tree(struct AST ast, const char* const qml_directory)
{
    struct dependency_tree_result_t result = {0};

    result.root.ast = ast;

    struct symbols_t symbols = find_symbols(ast);
    unique_symbols(&symbols);

    for(size_t i = 0; i < symbols.symbols_count; i++)
    {
        struct string_t qml_dir = string_from_char_array(qml_directory);
        string_append(&qml_dir, symbols.symbols[i].string);
        struct lex_parse_result_t r = lex_and_parse_from_file(qml_dir.string);

        if(r.error != 0)
        {
            result.error = 1;
            return result;
        }

        struct tree_node_t child = tree_push(result.root, r.ast);

        struct dependency_tree_result_t child_result = build_dependency_tree(r.ast, qml_directory);

        if(child_result.error != 0)
        {
            result.error = 1;
            return result;
        }
    }

    return result;
}

struct tranverse_result_t
{
    struct object_t root;
    int error;
};

static struct tranverse_result_t
tranverse_and_evaluate(struct object_t* root, struct tree_node_t tree, struct evaluated_t* evaluated_objects, size_t evaluated_objects_count, size_t evaluated_objects_cap)
{
    //eval
}

lol_handle
lol_init2(struct lol_config_t config, const char* const qml_directory, const char* const qml)
{
    struct lex_parse_result_t ast_result = lex_and_parse(qml);

    if(ast_result.error != 0)
    {
        return NULL;
    }

    struct AST main_ast = ast_result.ast;

    struct dependency_tree_result_t tree_result = build_dependency_tree(main_ast, qml_directory);

    if(tree_result.error != 0)
    {
        return NULL;
    }

    struct tree_node_t tree = tree_result.root;

    struct evaluated_t evaluated_objects[128] = {0};

    struct tranverse_result_t result = tranverse_and_evaluate(NULL, tree, evaluated_objects, 0, 128);

    if(result.error != 0)
    {
        return NULL;
    }

    struct lol_t* lol = malloc(sizeof(struct lol_t));
    memset(lol, 0, sizeof(struct lol_t));

    lol->config = config;
    lol->cat = cat_init2(lol->root_object);

    lol->root_object.cat = lol->cat;

    return lol;
}

lol_handle
lol_init(struct lol_config_t config, const char* const qml_directory, const char* const qml)
{
    struct lol_t* lol = malloc(sizeof(struct lol_t));
    memset(lol, 0, sizeof(struct lol_t));

    lol->config = config;

    size_t lexer_needs = lexer_memory_needed();
    size_t parser_needs = parser_memory_needed(1);

    size_t memory_needed = lexer_needs + parser_needs;

    lol->memory = lol_memory_init(memory_needed);

    struct memory_block_t lexer_memory = lol_alloc(lol->memory, lexer_needs);
    struct memory_block_t parser_memory = lol_alloc(lol->memory, parser_needs);

    lexer_handle lx = lexer_init(qml, lexer_memory);

    if(lx == -1)
    {
        fprintf(stderr, "lexer initialization failed\n");
        abort();
    }

    parser_handle p = parser_init(&lx, parser_memory, 1);

    struct AST ast = lol_parse(p);

    if(ast.error != 0)
    {
        fprintf(stderr, "parsing failed\n");
        lol_memory_deinit(lol->memory);
        free(lol);
        return NULL;
    }

    for(size_t i = 0; i < ast.count; i++)
    {
        printf("%s ", tokentype_to_string(ast.nodes[i].type));

        switch(ast.nodes[i].type)
        {
        case TOK_LOWERCASE_IDENTIFIER:
        case TOK_UPPERCASE_IDENTIFIER:
        case TOK_STRING:
        case TOK_INT_SYM:
        case TOK_REAL_SYM:
        case TOK_BOOL_SYM:
        case TOK_STRING_SYM:
            printf("%s", ast.nodes[i].value.string);
            break;
        case TOK_DOUBLE:
            printf("%f", ast.nodes[i].value.real);
            break;
        case TOK_INT:
            printf("%" PRIi64, ast.nodes[i].value.integer);
            break;
        default:
            break;
        }

        printf("\n");
    }

    fflush(stdout);

    lol->cat = cat_init(&lol->engine);

    lol->engine.cat = lol->cat;

    struct symbols_t symbols = find_symbols(ast, qml_directory);

    struct eval_result_t eval_result = eval(ast, &lol->engine, qml_directory);

    if(eval_result.error != 0)
    {
        fprintf(stderr, "evaluation failed\n");
        lol_memory_deinit(lol->memory);
        cat_free(lol->cat);
        return NULL;
    }

    // struct string_t missing = resolve_symbols(eval_result, qml_directory);

    // if(missing.length != 0)
    // {
    //     fprintf(stderr, "failed to resolve: %s\n", missing.string);
    //     lol_memory_deinit(lol->memory);
    //     cat_free(lol->cat);
    //     return NULL;
    // }

    lol_memory_deinit(lol->memory);

    return lol;
}

static int
get_object_under_mouse(struct engine_t engine, int mouse_x, int mouse_y)
{
    for(size_t i = 0; i < engine_object_count(engine); i++)
    {
        struct object_t object = engine.objects[i];

        if(object.type != Object_MouseArea)
        {
            continue;
        }

        int64_t x = engine_object_x(engine, i);
        int64_t y = engine_object_y(engine, i);
        int64_t w = engine_object_width(engine, i);
        int64_t h = engine_object_height(engine, i);

        if(cat_point_rect_hit_test(mouse_x, mouse_y, x, y, w, h))
        {
            return i;
        }
    }

    return -1;
}

void
lol_run(lol_handle lol)
{
    while(!cat_should_close(lol->cat))
    {
        int object_idx = get_object_under_mouse(lol->engine, cat_mouse_x(lol->cat), cat_mouse_y(lol->cat));
        int key = cat_key_press(lol->cat);

        struct event_t e = {
            .key_press = cat_key_press(lol->cat),
            .mouse_x = cat_mouse_x(lol->cat),
            .mouse_y = cat_mouse_y(lol->cat),
            .object_idx = object_idx,
            .mouse_down = cat_mouse_down(lol->cat, 0)
        };

        if(key)
        {
            lol->config.event_callback_function(lol->config.user_data, e);
        }
        else if(cat_mouse_down(lol->cat, 0))
        {
            e.type = Event_Mouse_Down;
            lol->config.event_callback_function(lol->config.user_data, e);
        }
        else if(cat_mouse_released(lol->cat, 0))
        {
            e.type = Event_Mouse_Released;
            lol->config.event_callback_function(lol->config.user_data, e);
        }
        else if(cat_mouse_pressed(lol->cat, 0))
        {
            e.type = Event_Mouse_Pressed;
            lol->config.event_callback_function(lol->config.user_data, e);
        }
        else
        {
            struct event_t e = {
                .key_press = cat_key_press(lol->cat),
                .mouse_x = cat_mouse_x(lol->cat),
                .mouse_y = cat_mouse_y(lol->cat),
                .type = Event_Mouse_Moved,
                .object_idx = object_idx
            };

            lol->config.event_callback_function(lol->config.user_data, e);
        }

        cat_update(lol->cat);
    }
}

struct engine_t
lol_engine(lol_handle lol)
{
    return lol->engine;
}

void
lol_release(lol_handle lol)
{
    cat_free(lol->cat);
    free(lol);
}
